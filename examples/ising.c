#include "config.h"
#include "latticeqft/latticeqft.h"
#include "ranlxs.h"
#include <mpi.h>
#include <string.h>
#include <time.h>

// call simulating functions and time
int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    srand(time(NULL) + rank); // different seeds for each task
    rlxs_init(1, rand());

    struct timespec begin, end, timed;

    ISING = 1;
    GAUGE = 0;

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &begin);
    int FAILED = 0;
    BLOCK *block =
        configure_block(argc, argv, rank, size,
                        &FAILED); // set up lattice and MPI communications
    if (FAILED) {
        goto error;
    }
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);

    timed.tv_sec = end.tv_sec - begin.tv_sec;
    timed.tv_nsec = end.tv_nsec - begin.tv_nsec;
    if (end.tv_nsec - begin.tv_nsec < 0) {
        timed.tv_nsec += 1000000000;
        timed.tv_sec -= 1;
    }

    // To create run-time file names
    char *prob_type;
    char *output = malloc(40 * sizeof(char));
    char *time_output = malloc(40 * sizeof(char));
    prob_type = "ising";

    sprintf(time_output, "data/%s-%dd-%d-time.txt", prob_type, block->DIMS,
            DIM[0]);
    FILE *file;
    if (!rank) {
        file = fopen(time_output, "w");
        fprintf(file, "test\n");
        fprintf(file, "DIMS = %d\ndim[0] = %d\n", block->DIMS, DIM[0]);
        fprintf(file, "MCsweeps = %d\nMetroSkips = %d\nTranskips = %d\n",
                MCsweeps, metroSkips, tranSkips);
        fprintf(file, "Configuration Time = %lld.%09ld seconds\n",
                (long long)timed.tv_sec, timed.tv_nsec);
    }

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &begin);
    sprintf(output, "data/%s-%dd-%d-data.txt", prob_type, block->DIMS, DIM[0]);

    run_ising_temperatureRange(block, MCsweeps, metroSkips, tranSkips, 1.0, 5.0,
                               0.1, output);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);

    timed.tv_sec = end.tv_sec - begin.tv_sec;
    timed.tv_nsec = end.tv_nsec - begin.tv_nsec;
    if (end.tv_nsec - begin.tv_nsec < 0) {
        timed.tv_nsec += 1000000000;
        timed.tv_sec -= 1;
    }
#ifdef NONBLOCKING
    MPI_Waitall(2 * block->DIMS, send_req, MPI_STATUSES_IGNORE);
    MPI_Waitall(2 * block->DIMS, send_req, MPI_STATUSES_IGNORE);
#endif
#ifdef PERSISTENT
    MPI_Waitall(2 * block->DIMS, send_req, MPI_STATUSES_IGNORE);
    MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
#endif

    if (!rank) // output timings
    {

        fprintf(file, "Calculation Time = %lld.%09ld seconds\n",
                (long long)timed.tv_sec, timed.tv_nsec);
        fclose(file);

        if (STRONG) {
            char *out = malloc(30 * sizeof(char));
            sprintf(out, "scaling/%s-%dd-%d-strong.txt", prob_type, block->DIMS,
                    DIM[0]);
            FILE *file2 = fopen(out, "aw");
            fprintf(file2, "%d\t%lld\n", size, (long long)timed.tv_sec);
            free(out);
            fclose(file2);
        }
        if (WEAK) {
            char *out = malloc(30 * sizeof(char));
            sprintf(out, "scaling/%s-%dd-weak.txt", prob_type, block->DIMS);
            FILE *file2 = fopen(out, "aw");
            fprintf(file2, "%d\t%lld\n", size, (long long)timed.tv_sec);
            free(out);
            fclose(file2);
        }
        if (DIMENSIONALITY) {
            char *out = malloc(30 * sizeof(char));
            sprintf(out, "scaling/%s-dims.txt", prob_type);
            FILE *file2 = fopen(out, "aw");
            fprintf(file2, "%d\t%lld\n", block->DIMS, (long long)timed.tv_sec);
            free(out);
            fclose(file2);
        }
    }

    // clean up
    free_mpi_data(block->DIMS);

    free_grid(block);

error:
    free(output);
    free(time_output);

    MPI_Finalize();
    return 0;
}
