#include "config.h"
#include "latticeqft/latticeqft.h"
#include <mpi.h>
#include <string.h>
#include <unistd.h>

// parse arguments and call all problem initialisatoin functions
BLOCK *configure_block(int argc, char *argv[], const int rank, const int size,
                       int *FAILED) {
    BLOCK *block = (BLOCK *)calloc(1, sizeof(BLOCK));

    block->h = 0.0;
    block->J = 1.0;
    block->T = 0.1;
    block->a = 1.0;
    block->DIMS = 2;
    kb = 1.0;
    block->beta = 1;
    block->buf_size = 1;

    MCsweeps = 100;
    metroSkips = 10;
    tranSkips = 10000;
    overrelaxes = 1;

    block->eps = 0.5;

    WEAK = 0;           // if(1), time is appended to scaling file
    STRONG = 0;         //
    DIMENSIONALITY = 0; //

    int DIM_SET = 0;
    int opt;
    while ((opt = getopt(argc, argv, "h:J:T:a:B:b:d:D:M:m:o:t:GSWP")) != -1) {
        int i;
        switch (opt) {
        case 'h': // specify h
            block->h = atof(optarg);
            break;
        case 'J': // specify J
            block->J = atof(optarg);
            break;
        case 'T': // specify once off temperature
            block->T = atof(optarg);
            break;
        case 'a': // specify lattice spacing
            block->a = atof(optarg);
            break;
        case 'B': // beta
            block->beta = atof(optarg);
            break;
        case 'b': // buffer size
            block->buf_size = atoi(optarg);
            break;
        case 'd': // parse dimensions
            block->DIMS = atoi(optarg);
            block->dim = malloc(block->DIMS * sizeof(int));
            MPI_DIM = malloc(block->DIMS * sizeof(int));
            DIM = malloc(block->DIMS * sizeof(int));
            for (i = 0; i < block->DIMS; i++) {
                DIM[i] = atoi(argv[optind + i]);
                MPI_DIM[i] = 0;
            }
            DIM_SET = 1;
            break;
        case 'D': // cubic special case for convenience
            block->DIMS = atoi(optarg);
            block->dim = malloc(block->DIMS * sizeof(int));
            DIM = malloc(block->DIMS * sizeof(int));
            MPI_DIM = malloc(block->DIMS * sizeof(int));
            DIM[0] = atoi(argv[optind]);
            MPI_DIM[0] = 0;
            for (i = 1; i < block->DIMS; i++) {
                DIM[i] = DIM[0];
                MPI_DIM[i] = 0;
            }
            DIM_SET = 1;
            break;
        case 'M': // Monte Carlo samples
            MCsweeps = atoi(optarg);
            break;
        case 'm': // Metropolis loops between samples
            metroSkips = atoi(optarg);
            break;
        case 'o': // Over-relaxation sweeps
            overrelaxes = atoi(optarg);
            break;
        case 't': // Transient skips
            tranSkips = atoi(optarg);
            break;
        case 'S': // Data used for strong scaling plot
            STRONG = 1;
            break;
        case 'W': // Data used for weak scaling plot
            WEAK = 1;
            break;
        case 'P': // Data used for dimensionality scaling plots
            DIMENSIONALITY = 1;
            break;
        case '?':
            fprintf(stderr, "no action for '-%c' flag\n", optopt);
            exit(1);
        }
    }
    block->B = 1.0 / (block->T * kb);
    if (!DIM_SET) // default dimensionality
    {
        block->dim = malloc(block->DIMS * sizeof(int));
        DIM = malloc(block->DIMS * sizeof(int));
        MPI_DIM = malloc(block->DIMS * sizeof(int));
        DIM[0] = 10;
        DIM[1] = 10;
        MPI_DIM[0] = 0;
        MPI_DIM[1] = 0;
    }

    create_topology(block);
    if (!rank) {
        printf("Splitting %d MPI tasks into %d", size, MPI_DIM[0]);
        int i;
        for (i = 1; i < block->DIMS; i++) {
            printf("X%d", MPI_DIM[i]);
        }
        printf(" grid...\n");
    }

    if (!rank) {
        printf("Creating a random grid with dimensions %d", DIM[0]);
        int i;
        for (i = 1; i < block->DIMS; i++) {
            printf("X%d", DIM[i]);
        }
        printf("...\n");
    }

    create_grid(block);

    // check if sub_lattices are compatible with checkerboard scheme
    int *OK_SUB_LATTICE_DIMENSIONS_CHECK = malloc(size * sizeof(int));
    MPI_Allgather(&OK_SUB_LATTICE_DIMENSIONS, 1, MPI_INT,
                  OK_SUB_LATTICE_DIMENSIONS_CHECK, 1, MPI_INT, MPI_COMM_WORLD);
    int i;
    for (i = 0; i < size; i++) {
        if (!OK_SUB_LATTICE_DIMENSIONS_CHECK[i] &&
            size > 0) // single processor lattice can have odd length sides of
                      // course
        {
            *FAILED = 1;
            if (!rank) {
                printf(
                    "Processor %d reported an odd sub-lattice side length...\n",
                    i);
            }
        }
        if (*FAILED) {
            if (!rank) {
                printf("This is not compatible with the checkerboard update "
                       "scheme...\nPlease provide arguments that result in "
                       "such sub-lattices.\n");
            }
            free(OK_SUB_LATTICE_DIMENSIONS_CHECK);
            return block;
        }
    }
    free(OK_SUB_LATTICE_DIMENSIONS_CHECK);
    randomise_grid(block);

    set_up_comms(block);

    return block;
}
