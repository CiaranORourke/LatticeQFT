#include "../src/simulation/algorithms.h"

void ergodic_test_metropolis(BLOCK *block, const int transkips, const int N,
                             const int reps);
void ergodic_test_metropolis_with_overrelax(BLOCK *block, const int transkips,
                                            const int N, const int reps);
void test_parallel_jackknife(BLOCK *block, const int N, const int metro);
void test_overrelaxation_invariance(BLOCK *block);
