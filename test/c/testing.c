#include "testing.h"

// N is the number of samples in the test
void ergodic_test_metropolis(BLOCK *block, const int transkips, const int N,
                             const int reps) {
    randomise_grid(block);
    transient(block, transkips);
    float *global_prob_dist;
    if (!rank) {
        global_prob_dist = malloc(N * sizeof(float));
    }
    float *loc_prob_dist = malloc(N * sizeof(float));
    int i;
    for (i = 0; i < N; i++) {
        loc_prob_dist[i] = metropoloop(block, reps, 1);
    }
    // note that MPI_Reduce adds corresponding indices of arrays
    MPI_Reduce(loc_prob_dist, global_prob_dist, N, MPI_FLOAT, MPI_SUM, 0,
               cartcomm);
    free(loc_prob_dist);
    if (!rank) {
        char *output = malloc(40 * sizeof(char));
        if (ISING) {
            sprintf(output, "test/ising-%dd-%d-%.1f-metro.txt", block->DIMS,
                    DIM[0], block->T);
        }
        if (GAUGE) {
            sprintf(output, "test/gauge-%dd-%d-metro.txt", block->DIMS, DIM[0]);
        }
        FILE *file = fopen(output, "w");
        fprintf(file, "config\tprob_dist\n");

        float prob_mean = 0;
        int i;
        for (i = 0; i < N; i++) {
            global_prob_dist[i] /= (float)size;
            prob_mean += global_prob_dist[i];
            fprintf(file, "%d\t%f\n", i + 1, global_prob_dist[i]);
        }
        prob_mean /= (float)N;
        float jack = jackknife(global_prob_dist, N, prob_mean);
        printf("Distribution mean = %f, variance = %f\n", prob_mean, jack);
        free(global_prob_dist);
        free(output);
    }
}

// NEED TO CHANGE RED-BLACK SCHEME
void ergodic_test_metropolis_with_overrelax(BLOCK *block, const int transkips,
                                            const int N, const int reps) {
    randomise_grid(block);
    transient(block, transkips);
    int *rb_pick = malloc(reps * sizeof(int));
    if (!rank) {
        int i;
        for (i = 0; i < reps; i++) {
            rb_pick[i] = rand() % 2;
        }
    }
    MPI_Bcast(rb_pick, reps, MPI_INT, 0, cartcomm);

    int k;
    for (k = 0; k < reps; k++) {
        if (rb_pick[k]) {
            int i;
            for (i = 0; i < block->points; i++) {
                int pt = 0;
                int j;
                for (j = 0; j < block->DIMS; j++) {
                    int mv = 1 + rand() % (block->dim[j] - 2);
                    pt += mv * block->traverse[j];
                }
                float *phi_sum =
                    staples(block, pt / block->DIMS, pt % block->DIMS);
                float staple_re_sum = 0;
                float staple_im_sum = 0;
                int l;
                for (l = 0; l < block->DIMS; l++) {
                    float re;
                    float im;
                    if (l != pt % block->DIMS) {
                        phase_to_z(phi_sum[2 * l], &re, &im);
                        staple_re_sum += re;
                        staple_im_sum += im;
                        phase_to_z(phi_sum[2 * l + 1], &re, &im);
                        staple_re_sum += re;
                        staple_im_sum += im;
                    }
                }
                free(phi_sum);
                float alpha;
                z_to_phase(&alpha, staple_re_sum, staple_im_sum);
                block->link[pt].phi =
                    2.0 * M_PI - 2.0 * alpha - block->link[pt].phi; // update
            }
        } else if (block->dim[0] > 4) {
            int i;
            for (i = 0; i < block->points; i++) {
                int pt = 0;
                int j;
                for (j = 0; j < block->DIMS; j++) {
                    int mv = 2 + rand() % (block->dim[j] - 4);
                    pt += mv * block->traverse[j];
                }
                float *phi_sum =
                    staples(block, pt / block->DIMS, pt % block->DIMS);
                float staple_re_sum = 0;
                float staple_im_sum = 0;
                int l;
                for (l = 0; l < block->DIMS; l++) {
                    float re;
                    float im;
                    if (l != pt % block->DIMS) {
                        phase_to_z(phi_sum[2 * l], &re, &im);
                        staple_re_sum += re;
                        staple_im_sum += im;
                        phase_to_z(phi_sum[2 * l + 1], &re, &im);
                        staple_re_sum += re;
                        staple_im_sum += im;
                    }
                }
                free(phi_sum);
                float alpha;
                z_to_phase(&alpha, staple_re_sum, staple_im_sum);
                block->link[pt].phi =
                    2.0 * M_PI - 2.0 * alpha - block->link[pt].phi; // update
            }
        } else if (block->dim[0] > 4) {
            int i;
            for (i = 0; i < block->points; i++) {
                int pt = 0;
                int j;
                for (j = 0; j < block->DIMS; j++) {
                    int mv = 2 + rand() % (block->dim[j] - 4);
                    pt += mv * block->traverse[j];
                }
                float *phi_sum =
                    staples(block, pt / block->DIMS, pt % block->DIMS);
                float staple_re_sum = 0;
                float staple_im_sum = 0;
                int l;
                for (l = 0; l < block->DIMS; l++) {
                    float re;
                    float im;
                    if (l != pt % block->DIMS) {
                        phase_to_z(phi_sum[2 * l], &re, &im);
                        staple_re_sum += re;
                        staple_im_sum += im;
                        phase_to_z(phi_sum[2 * l + 1], &re, &im);
                        staple_re_sum += re;
                        staple_im_sum += im;
                    }
                }
                free(phi_sum);
                float alpha;
                z_to_phase(&alpha, staple_re_sum, staple_im_sum);
                block->link[pt].phi =
                    2.0 * M_PI - 2.0 * alpha - block->link[pt].phi; // update
            }
        }
        transfer_all_faces(block);
    }
    free(rb_pick);
}

void test_overrelaxation_invariance(BLOCK *block) {
    if (!rank) {
        /*
        float *neg_sums = staples(block, 5, 0);
        float *pos_sums = staples(block, 5, 1);
        printf("neg sums = %f, %f\n", block->link[5*block->DIMS].phi +
        neg_sums[2], block->link[5*block->DIMS].phi + neg_sums[3]); printf("pos
        sums = %f, %f\n", block->link[5*block->DIMS + 1].phi + pos_sums[0],
        block->link[5*block->DIMS + 1].phi + pos_sums[1]);
        */
        // printf("Before = %f\n", block_S(block));

        int count = 0;
        int i;
        for (i = 0; i < block->DIMS * block->points; i++) {
            if (!block->buf[i / block->DIMS]) {
                float loc_act =
                    S(block, i / block->DIMS, i % block->DIMS, 0, 1);
                printf("%f\t", loc_act);
                if ((count / block->DIMS + 1) % (block->dim[0] - 2) == 0 &&
                    i % block->DIMS == block->DIMS - 1) {
                    printf("\n");
                }
                count++;
            }
        }
        printf("\n");

        int point = rand() % block->DIMS;
        int j;
        for (j = 0; j < block->DIMS; j++) {
            int mv = 1 + rand() % (block->dim[j] - 2);
            point += mv * block->traverse[j];
        }
        printf("point = %d\n", point);
        int pt = point / block->DIMS;
        int lk = point % block->DIMS;

        float *phi_sum = nXn_staples(block, pt, lk, block->buf_size);
        // printf("phi_sum2 delivered %f\n", phi_sum2[0]);
        // float *phi_sum = staples(block, pt, lk);

        float staple_re_sum = 0;
        float staple_im_sum = 0;
        int l;
        for (l = 0; l < block->DIMS; l++) {
            float re;
            float im;
            if (l != lk) {
                phase_to_z(phi_sum[2 * l], &re, &im);
                staple_re_sum += re;
                staple_im_sum += im;
                phase_to_z(phi_sum[2 * l + 1], &re, &im);
                staple_re_sum += re;
                staple_im_sum += im;
            }
        }
        free(phi_sum);
        float alpha;
        z_to_phase(&alpha, staple_re_sum, staple_im_sum);
        float bef = S(block, pt, lk, 0, 0);
        block->link[point].phi =
            2.0 * M_PI - 2.0 * alpha - block->link[point].phi;
        float aft = S(block, pt, lk, 0, 0);

        printf("%f -> %f\n", bef, aft);

        count = 0;
        for (i = 0; i < block->DIMS * block->points; i++) {
            if (!block->buf[i / block->DIMS]) {
                float loc_act =
                    S(block, i / block->DIMS, i % block->DIMS, 0, 1);
                printf("%f\t", loc_act);
                if ((count / block->DIMS + 1) % (block->dim[0] - 2) == 0 &&
                    i % block->DIMS == block->DIMS - 1) {
                    printf("\n");
                }
                count++;
            }
        }

        // printf("After = %f\n", block_S(block));
    }
    transfer_all_faces(block);
}
