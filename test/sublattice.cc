#include <catch2/catch.hpp>

#include <latticeqft/latticeqft.h>
#include <latticeqft/sublattice.h>

namespace latticeqft {
namespace sublattice {

TEMPLATE_TEST_CASE_SIG("Sublattice construction for different ranks", "[sanity][template]", ((size_t rank), rank), (1), (2), (3), (4))
{
    GIVEN("a field defined on vertices")
    {
        std::vector<int> sizes(rank);
        for (int i = 0; i < rank; i++) {
            sizes[i] = 1 + rand()%10;
        }

        WHEN("a Sublattice is constructed with the field")
        {
            Sublattice<rank, field::Ising_variable> sublattice(std::make_unique<field::Ising<rank>>(), sizes);

            THEN("the Sublattice reflects to input dimensions")
            {
                for (int i = 0; i < rank; i++) {
                    REQUIRE(sublattice.dimension_size(i) == sizes[i]);
                }
            }
        }
    }
    GIVEN("a field defined on edges")
    {
        std::vector<int> sizes(rank);
        for (int i = 0; i < rank; i++) {
            sizes[i] = 1 + rand()%10;
        }

        WHEN("a Sublattice is constructed with the field")
        {
            Sublattice<rank, field::Gauge_variable> sublattice(std::make_unique<field::Gauge<rank>>(), sizes);

            THEN("the Sublattice reflects to input dimensions")
            {
                for (int i = 0; i < rank; i++) {
                    REQUIRE(sublattice.dimension_size(i) == sizes[i]);
                }
            }
        }
    }
    GIVEN("fields defined on vertices and edges")
    {
        std::vector<int> sizes(rank);
        for (int i = 0; i < rank; i++) {
            sizes[i] = 1 + rand()%10;
        }

        WHEN("a Sublattice is constructed with the fields")
        {
            Sublattice<rank, field::Ising_variable, field::Gauge_variable> sublattice(std::make_unique<field::Ising<rank>>(), std::make_unique<field::Gauge<rank>>(), sizes);

            THEN("the Sublattice reflects to input dimensions")
            {
                for (int i = 0; i < rank; i++) {
                    REQUIRE(sublattice.dimension_size(i) == sizes[i]);
                }
            }
        }
    }
}

}  // namespace sublattice 
}  // namespace latticeqft
