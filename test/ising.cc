#include <catch2/catch.hpp>

#include <latticeqft/field/ising.h>

/* TODO temp */
#include <iostream>

namespace latticeqft {
namespace field {

TEMPLATE_TEST_CASE_SIG("Ising sublattice member functions for different ranks", "[template]", ((size_t rank), rank), (1), (2), (3), (4))
{
    GIVEN("an Ising field")
    {
        std::vector<int> sizes(rank);
        for (int i = 0; i < rank; i++) {
            /* account for default buffer_width of 1 */
            sizes[i] = 3 + (rand()%10);
        }

        Ising<rank> field(sizes);
        float H;

        WHEN("the field is initialised")
        {
            field.initialise();
            THEN("the field is randomised")
            {
                for (int i = 0; i < field.size(); i++) {
                    REQUIRE((field.data(i).spin == -1 || field.data(i).spin == 1));
                }
            }
            THEN("the field has a sane Hamiltonian")
            {
                H = field.hamiltonian();
                REQUIRE((H < 4.0*rank*field.size() && H > -4.0*rank*field.size()));
            }
            AND_WHEN("the field is updated")
            {
                for (int i = 0; i < 10; i++) {
                    field.update();
                }
                /* TODO nondeterministic */
                THEN("the Hamiltonian changes")
                {
                    REQUIRE(field.hamiltonian() != H);
                }
            }
        }
    }
}

}  // namespace field
}  // namespace latticeqft
