#include <catch2/catch.hpp>

#include <latticeqft/field/nd_vector.h>

namespace latticeqft {
namespace field {

TEMPLATE_TEST_CASE_SIG("Nd_vector construction and sizing for different types and ranks", "[sanity][template]", ((typename T, size_t rank), T, rank), (char, 1), (int, 2), (double, 3), (std::vector<float>, 4))
{
    GIVEN("an Nd_vector")
    {
        int sizes[rank];
        for (int i = 0; i < rank; i++) {
            sizes[i] = 1 + rand()%10;
        }

        WHEN("an Nd_vector is constructed from a std::vector of dimension sizes")
        {
            std::vector<int> sizes_vec(rank);
            for (int i = 0; i < rank; i++) {
                sizes_vec[i] = sizes[i];
            }

            Nd_vector<T, rank> vec(sizes_vec);
            REQUIRE(vec.rank() == rank);

            THEN("the Nd_vector takes on those dimensions")
            {
                for (int i = 0; i < rank; i++) {
                    REQUIRE(vec.dimension_size(i) == sizes[i]);
                }
            }
            AND_WHEN("the Nd_vector is copy constructed")
            {
                Nd_vector<T, rank> vec2(vec);
                AND_THEN("the new Nd_vector takes on those dimensions")
                {
                    for (int i = 0; i < rank; i++) {
                        REQUIRE(vec2.dimension_size(i) == sizes[i]);
                    }
                }
            }
            AND_WHEN("the Nd_vector is assignment constructed")
            {
                Nd_vector<T, rank> vec2 = vec;
                AND_THEN("the new Nd_vector takes on those dimensions")
                {
                    for (int i = 0; i < rank; i++) {
                        REQUIRE(vec2.dimension_size(i) == sizes[i]);
                    }
                }
            }
        }
        WHEN("an Nd_vector is constructed from a std::array of dimension sizes")
        {
            std::array<int, rank> sizes_arr;
            for (int i = 0; i < rank; i++) {
                sizes_arr[i] = sizes[i];
            }

            Nd_vector<T, rank> vec(sizes_arr);
            REQUIRE(vec.rank() == rank);

            THEN("the Nd_vector takes on those dimensions")
            {
                for (int i = 0; i < rank; i++) {
                    REQUIRE(vec.dimension_size(i) == sizes[i]);
                }
            }

        }
        WHEN("the Nd_vector is given dimensions using resize")
        {
            std::array<int, rank> sizes_arr;
            for (int i = 0; i < rank; i++) {
                sizes_arr[i] = sizes[i];
            }

            Nd_vector<T, rank> vec(sizes_arr);
            REQUIRE(vec.rank() == rank);

            THEN("the Nd_vector takes on those dimensions")
            {
                for (int i = 0; i < rank; i++) {
                    REQUIRE(vec.dimension_size(i) == sizes[i]);
                }
            }

        }
    }
}

}  // namespace field 
}  // namespace latticeqft
