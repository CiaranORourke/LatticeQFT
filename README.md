# LatticeQFT

[description]

## Build Intructnions

To configure and build LatticeQFT:

```
# Create a directory to hold the temporary build files
mkdir -p build

# Change directory to the build directory
cd build

# Configure the LatticeQFT build using CMake
cmake \
    [see options below] \
    ..

# Build the LatticeQFT project
make -C build

# Install the LatticeQFT project
make -C build install

```

CMake should find the necessary MPI library automatically. It also searches for the C compiler in the C environment variable.

### Useful Options
Option | Effect
------ | ------
`-DCMAKE_C_COMPILER=...` | Set the C compiler
`-DCMAKE_C_FLAGS=...`    | Set the flags to pass to the C compiler. Overrides the default flags.
`-DCMAKE_BUILD_TYPE=...` | Set to `Release` or `Debug` for release or debug builds.

#### Dependencies
Option | Effect
------ | ------
`-DMPI_C_COMPILER=...` | Set the MPI C compiler wrapper (i.e mpicc).

#### Enabling / Disabling Sections of LatticeQFT
Option | Effect
------ | ------
`-DLATTICEQFT_BUILD_EXAMPLES=...` | Set to `ON` to build LatticeQFT example programs or `OFF` to skip (Default `OFF`).

#### Installation Directories
Option | Effect
------ | ------
`-DCMAKE_INSTALL_PREFIX=...` | Set the root install directory for the compiled libraries and programs.
`-DCMAKE_INSTALL_BINDIR=...` | Set the install directory for LatticeQFT executables. Use a relative path to set the path relative to `${CMAKE_INSTALL_PREFIX}` (Default `bin`).

