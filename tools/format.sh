#!/bin/sh

set -o errexit

# Find clang-format executable
: ${CLANG_FORMAT_EXECUTABLE:=$(which clang-format)}
export CLANG_FORMAT_EXECUTABLE

file="$1"
tmpfile="$(mktemp)"

# Clean up tmpfile on exit
trap "rm -f \"${tmpfile}\"" EXIT

# Run clang-format over file, writing changes to tmpfile
cat "${file}" \
    | sed 's|#pragma omp|//@#pragma omp|' \
    | ${CLANG_FORMAT_EXECUTABLE} -style=file \
    | sed 's|//@#pragma omp|#pragma omp|' \
    > "${tmpfile}"

if [ ! -s "${tmpfile}" ]; then
    echo "Error! Formatted file is empty!"
    exit 1
fi


# Swap file for tmpfile if there is a change
set +o errexit
if cmp --quiet "${tmpfile}" "${file}"; then
    set -o errexit
    echo "Formatting ${file}: No Change..."
    rm "${tmpfile}"
else
    set -o errexit
    echo "Formatting ${file}: Updating..."
    mv "${tmpfile}" "${file}"
fi
