#!/bin/sh

#SBATCH -n 16
#SBATCH -t 02:00:00
#SBATCH -p compute 
#SBATCH -J 2d-gauge-par

module load cports openmpi
#module load cports6 openmpi/1.6.5-mpi-thread-gnu4.8.2
make

mpirun -n 16 ./lat -D 2 4096
#mpirun -n 16 ./lat -D 4 20 -t 100 -m 3 -G 

#mpirun -n 16 valgrind --suppressions=MPI_supressions.supp --tool=memcheck --leak-check=full --track-origins=yes ./lat -D 2 1024

#mpirun valgrind --tool=memcheck --leak-check=full ./lat -D 2 4

#mpirun valgrind --gen-suppressions=all --suppressions=MPI_supressions.supp --tool=memcheck --leak-check=full --track-origins=yes ./lat -D 2 4 

make clean

module purge
