#!/bin/sh

set -e

if [[ $# -lt 2 ]]; then
    echo "usage; $(basename "$0") BUILD_DIRECTORY TARGET_FILE"

    exit 1
fi

build_dir="$1"
target_file="$2"
shift 2

# Assume this file is in scripts/
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source_dir="${script_dir}/.."

echo "Linting (clang-tidy) ${target_file}"
pushd ${source_dir} > /dev/null
clang-tidy "${target_file}" \
    -p "${build_dir}" \
    --header-filter="${source_dir}" \
    --quiet \
    "$@"
popd > /dev/null
