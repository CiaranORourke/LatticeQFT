#!/bin/sh

nprocs=1

set -o errexit

# Get build directory from the command line
if [[ $# -ge 1 ]]; then
    # get absolute path of build directory
    build_dir="$(cd "$1" && pwd)"
    shift
    echo
    echo "BUILD_DIRECTORY: ${build_dir}"
    echo
else
    echo
    echo "Error: wrong number of arguments"
    echo
    echo "Usage: $(basename $0) BUILD_DIRECTORY"
    echo 

    exit 1
fi

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source_dir="${script_dir}/.."

# Directories containing source files
source_dirs="src test examples"

# Run Lint.sh on each source file
cd "${source_dir}"
find ${source_dirs} \( \
    -iname "*.c" -o -iname "*.cc" \
    \) -print0 \
    | xargs -0 -n 1 -P "${nprocs}" -I TARGET_FILE "${script_dir}/lint.sh" "${build_dir}" TARGET_FILE "$@"
