#!/bin/sh

nprocs=1
set -o errexit

# Assume current script is in scripts/
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source_dir="${script_dir}/.."

# Get the format script
FORMAT_EXECUTABLE="${script_dir}/format.sh"

cd ${source_dir}

# Directories containing source files
source_dirs="src test examples"

# Source file extensions
source_exts="c h"

for dir in $source_dirs; do
    for ext in $source_exts; do
        if [ ! -z "$(find $dir -iname "*.${ext}")" ]; then
            find $dir -iname "*.${ext}" -print0 \
                | xargs -n 1 -0 -P "${nprocs}" ${FORMAT_EXECUTABLE}
        fi
    done
done
