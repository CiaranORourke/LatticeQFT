#include "block.h"

//#define NXN

void phase_to_z(const float phi, float *re, float *im);
void z_to_phase(float *phi, const float re, const float im);
float *staples(BLOCK *block, const int pnt, const int lnk);
float *nXn_staples(BLOCK *block, const int pnt, const int lnk, const int N);
float S(BLOCK *block, const int pnt, const int lnk, int next,
        int directional); // point action U(1)
float update(BLOCK *block, const int pnt, const int lnk, float rng);
float block_S(BLOCK *block);
