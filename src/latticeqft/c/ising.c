#include "ising.h"
#include <math.h>

// returns Hamiltonian at site loc
// dE = 1 returns the energy change caused by flipping the spin at site loc
float H(BLOCK *block, int point, const int dE) {
    // need to sum nearest neighbours
    float spin_sum = 0;

    // sum over nearest neighbour spin
    int i;
    for (i = 0; i < 2 * block->DIMS; i++) {
        // do this better, maybe store pointers to sites instead on indexes
        spin_sum += block->site[block->site[point].dir[i]].spin;
    }
    int loc_spin = block->site[point].spin;
    float H1 = -1.0 * block->J * loc_spin * (spin_sum)-block->h * loc_spin;
    if (dE) {
        return 1.0 * block->J * loc_spin * (spin_sum) + block->h * loc_spin -
               H1;
    }
    return H1;
}

// The update function for the Ising model sites
// changes the spin according to the Boltzmann probability distribution
// rng is taken as an argument to reduce calls to rn generation
float flip_or_not(BLOCK *block, int point, float rng) {
    float dE = H(block, point, 1);
    float prob = exp(-1.0 * block->B * dE);
    if (rng < prob) {
        block->site[point].spin *= -1;
    }
    return prob;
}

// returns the contribution to the total energy of the sublattice
float block_energy(BLOCK *block) {
    float E = 0.0;
    int i;
    for (i = 0; i < block->points; i++) {
        if (!block->buf[i]) // don't include buffer regions
        {
            E += H(block, i, 0);
        }
    }
    return E / 2.0; // each nearest neighbour interaction was counted twice
}

// returns magetisation (sum of spins) of this sublattice
float block_magnetisation(BLOCK *block) {
    int M = 0;
    int i;
    for (i = 0; i < block->points; i++) {
        if (!block->buf[i]) {
            M += block->site[i].spin;
        }
    }
    // M = abs(M);
    return (float)M; // float as will be averaged
}
