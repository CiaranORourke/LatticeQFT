#include "block.h"

float H(BLOCK *block, int point, const int dE);
float flip_or_not(BLOCK *block, int point, float rng);
float block_energy(BLOCK *block);
float block_magnetisation(BLOCK *block);
