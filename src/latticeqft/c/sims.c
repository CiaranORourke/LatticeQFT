#include "algorithms.h"
#include "comms.h"
#include "gauge.h"
#include "ising.h"
#include "latticeqft/latticeqft.h"
#include "sims.h"
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

// records change of energy and magnetisation with temperature to 'filename'
void run_ising_temperatureRange(BLOCK *block, const int MCsweeps,
                                const int metroSkips, const int transkips,
                                float T1, float T2, float dT, char *filename) {
    // fprintf(file2, "Energy\n");

    FILE *file;

    float *E_loc = malloc(MCsweeps * sizeof(float));
    float *M_loc = malloc(MCsweeps * sizeof(float));
    float *E_block;
    float *M_block;
    if (!rank) {
        file = fopen(filename, "w");
        fprintf(file,
                "Temperature\tEnergy\t\tVariance\tMagnetisation\tVariance\n");
        E_block = malloc(MCsweeps * sizeof(float));
        M_block = malloc(MCsweeps * sizeof(float));
    }
    block->T = T1;
    while (block->T <= T2) {
        block->B = 1.0 / (block->T * kb);
        randomise_grid(block);

        transient(block, transkips);

        int i;
        for (i = 0; i < MCsweeps; i++) // Monte Carlo Loop
        {
            metropoloop(block, metroSkips, 1);
#ifdef NONBLOCKING
            MPI_Waitall(
                2 * block->DIMS, recv_req,
                MPI_STATUSES_IGNORE); // need all buffers for system energy
#endif
#ifdef PERSISTENT
            MPI_Waitall(2 * block->DIMS, send_req, MPI_STATUSES_IGNORE);
            MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
#endif

            E_loc[i] = block_energy(block);
            // E += E_loc[i];
            M_loc[i] = block_magnetisation(block);
        }

        // note that MPI_Reduce adds corresponding positions of send buffers
        MPI_Reduce(E_loc, E_block, MCsweeps, MPI_FLOAT, MPI_SUM, 0, cartcomm);
        MPI_Reduce(M_loc, M_block, MCsweeps, MPI_FLOAT, MPI_SUM, 0, cartcomm);

        if (!rank) {
            float E = 0;
            float M = 0;

            int j;
            for (j = 0; j < MCsweeps; j++) {
                E += E_block[j];
                M_block[j] = fabsf(M_block[j]);
                M += M_block[j];
            }
            E /= (float)MCsweeps;
            M /= (float)MCsweeps;

            float E_var = jackknife(E_block, MCsweeps, E);
            float M_var = jackknife(M_block, MCsweeps, M);

            fprintf(file, "%f\t%f\t%f\t%f\t%f\n", block->T, E, E_var, M, M_var);
        }

        block->T += dT;
    }
    free(E_loc);
    free(M_loc);
    if (!rank) {
        free(E_block);
        free(M_block);
        fclose(file);
    }
}

// results of the simulation are action and change of action against MC time
void run_gauge_actionVtime(BLOCK *block, const int T, const int metro,
                           const int relax, const int multi) {
    randomise_grid(block);
    transfer_all_faces(block);

    float *loc_action_time = malloc(T * sizeof(float));
    loc_action_time[0] = block_S(block);
    loc_action_time[0] /= (float)(block->DIMS) * (float)block->points;
    int t;
    for (t = 1; t < T; t++) {
        metropoloop(block, metro, multi);
#ifdef NONBLOCKING
        MPI_Waitall(2 * block->DIMS, send_req,
                    MPI_STATUSES_IGNORE); // need all buffers for system energy
        MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
#endif
#ifdef PERSISTENT
        MPI_Waitall(2 * block->DIMS, send_req, MPI_STATUSES_IGNORE);
        MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
#endif
        overrelax(block, relax);
        loc_action_time[t] = 2.0 * block_S(block);
        loc_action_time[t] /= (float)(block->DIMS) * (float)(block->DIMS - 1) *
                              (float)block->points;
    }
    float *action_time;
    if (!rank) {
        action_time = malloc(T * sizeof(int));
    }
    MPI_Reduce(loc_action_time, action_time, T, MPI_FLOAT, MPI_SUM, 0,
               cartcomm);
    free(loc_action_time);

    if (!rank) {
        float *dS = malloc(T * sizeof(float));
        FILE *file = fopen("data/gauge_sim.txt", "w");
        fprintf(file, "Time\tAction\tDelta\n");
        action_time[0] /= (float)size;
        dS[0] = 0.0; // oh well, why not?
        fprintf(file, "%d\t%f\t%f\n", t, action_time[0], dS[0]);
        for (t = 1; t < T; t++) {
            action_time[t] /= (float)size;
            dS[t] = action_time[t] - action_time[t - 1];
            fprintf(file, "%d\t%f\t%f\n", t, action_time[t], dS[t]);
        }
        free(action_time);
        free(dS);
        fclose(file);
    }
}
