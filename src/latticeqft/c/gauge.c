#include "gauge.h"
#include <math.h>
#include <stdlib.h>

// determines cos and sin from phi
void phase_to_z(const float phi, float *re, float *im) {
    *re = cos(phi);
    *im = sin(phi);
}

// determines phi from real and imaginary parts
void z_to_phase(float *phi, const float re, const float im) {
    *phi = atan2(im, re);
    if (*phi < 0) {
        *phi += 2 * M_PI;
    }
}

// returns sum of each of the staples an an array
float *staples(BLOCK *block, const int pnt, const int lnk) {
    // Each dim beyond the first introduces two plaquettes

    float *phi_sum = malloc(2 * block->DIMS * sizeof(float));
    int i;
    for (i = 0; i < block->DIMS; i++) {
        phi_sum[2 * i] = 0.0;
        phi_sum[2 * i + 1] = 0.0;
        if (i != lnk) // no staple in orientation of lnk
        {
            // in order of direction of plaquette direction;
            // U_-n(x + m) = *U_n(x + m - n)
            phi_sum[2 * i] -=
                block
                    ->link[pnt * block->DIMS + block->traverse[lnk] -
                           block->traverse[i] + i]
                    .phi;
            // U_n(x + m)
            phi_sum[2 * i + 1] +=
                block->link[pnt * block->DIMS + block->traverse[lnk] + i].phi;

            // *U_m(x - n)
            phi_sum[2 * i] -=
                block->link[pnt * block->DIMS - block->traverse[i] + lnk].phi;
            // *U_m(x + n)
            phi_sum[2 * i + 1] -=
                block->link[pnt * block->DIMS + block->traverse[i] + lnk].phi;

            // *U_-n(x) = U_n(x - n)
            phi_sum[2 * i] +=
                block->link[pnt * block->DIMS - block->traverse[i] + i].phi;
            // *U_n(x)
            phi_sum[2 * i + 1] -= block->link[pnt * block->DIMS + i].phi;

#ifdef stap_debug
            printf("i = %d, pnt = %d, lnk = %d\n", i, pnt, lnk);
            printf("self = %d, sum = %f\n", pnt * block->DIMS + lnk,
                   block->link[pnt * block->DIMS + lnk].phi);
            printf("-1 = %d, sum = %f\n",
                   pnt * block->DIMS + block->traverse[lnk] -
                       block->traverse[i] + i,
                   block
                       ->link[pnt * block->DIMS + block->traverse[lnk] -
                              block->traverse[i] + i]
                       .phi);
            printf(
                "-2 = %d, sum = %f\n",
                pnt * block->DIMS - block->traverse[i] + lnk,
                block->link[pnt * block->DIMS - block->traverse[i] + lnk].phi);
            printf("-3 = %d, sum = %f\n",
                   pnt * block->DIMS - block->traverse[i] + i,
                   block->link[pnt * block->DIMS - block->traverse[i] + i].phi);
            printf(
                "+1 = %d, sum = %f\n",
                pnt * block->DIMS + block->traverse[lnk] + i,
                block->link[pnt * block->DIMS + block->traverse[lnk] + i].phi);
            printf(
                "+2 = %d, sum = %f\n",
                pnt * block->DIMS + block->traverse[i] + lnk,
                block->link[pnt * block->DIMS + block->traverse[i] + lnk].phi);
            printf("+3 = %d, sum = %f\n", pnt * block->DIMS + i,
                   block->link[pnt * block->DIMS + i].phi);
#endif
        }
    }
    return phi_sum;
}

// same as above but for arbitrary sized Wilson loops
float *nXn_staples(BLOCK *block, const int pnt, const int lnk, const int N) {
    // Each dim beyond the first introduces two plaquettes

    float *phi_sum = malloc(2 * block->DIMS * sizeof(float));
    int i;
    for (i = 0; i < block->DIMS; i++) {
        phi_sum[2 * i] = 0.0;
        phi_sum[2 * i + 1] = 0.0;
        if (i != lnk) {
            // in order of plaquette direction;
            int j;
            for (j = 0; j < N; j++) // sum over length of Wilson loop sides
            {
                if (j != 0) // extending from self
                {
                    // U_m(x + jm)
                    phi_sum[2 * i] += block
                                          ->link[pnt * block->DIMS +
                                                 j * block->traverse[lnk] + lnk]
                                          .phi;
                    phi_sum[2 * i + 1] +=
                        block
                            ->link[pnt * block->DIMS +
                                   j * block->traverse[lnk] + lnk]
                            .phi;
                }
                // U_-(j+1)n(x + Nm) = *U_n(x + Nm - (j+1)n)
                phi_sum[2 * i] -=
                    block
                        ->link[pnt * block->DIMS + N * block->traverse[lnk] -
                               (j + 1) * block->traverse[i] + i]
                        .phi;
                // U_(j+1)n(x + Nm) = U_n(x + Nm + jn)
                phi_sum[2 * i + 1] +=
                    block
                        ->link[pnt * block->DIMS + N * block->traverse[lnk] +
                               j * block->traverse[i] + i]
                        .phi;

                // *U_(j+1)m(x - Nn) = *U_m(x - Nn + jm)
                phi_sum[2 * i] -=
                    block
                        ->link[pnt * block->DIMS - N * block->traverse[i] +
                               j * block->traverse[lnk] + lnk]
                        .phi;
                // *U_(j+1)m(x + Nn) = *U_m(x + Nn + jm)
                phi_sum[2 * i + 1] -=
                    block
                        ->link[pnt * block->DIMS + N * block->traverse[i] -
                               j * block->traverse[lnk] + lnk]
                        .phi;

                // *U_-(j+1)n(x) = U_n(x - jn)
                phi_sum[2 * i] += block
                                      ->link[pnt * block->DIMS -
                                             (j + 1) * block->traverse[i] + i]
                                      .phi;
                // *U_(j+1)n(x) = *U_n(x + jn)
                phi_sum[2 * i + 1] -=
                    block->link[pnt * block->DIMS + j * block->traverse[i] + i]
                        .phi;

#ifdef stap_debug
                printf("GEN: i = %d, pnt = %d, lnk = %d\n", i, pnt, lnk);
                printf("self = %d, sum = %f\n", pnt * block->DIMS + lnk,
                       block->link[pnt * block->DIMS + lnk].phi);
                printf("-1 = %d, sum = %f\n",
                       pnt * block->DIMS + N * block->traverse[lnk] -
                           (j + 1) * block->traverse[i] + i,
                       block
                           ->link[pnt * block->DIMS + N * block->traverse[lnk] -
                                  (j + 1) * block->traverse[i] + i]
                           .phi);
                printf("-2 = %d, sum = %f\n",
                       pnt * block->DIMS - N * block->traverse[i] +
                           j * block->traverse[lnk] + lnk,
                       block
                           ->link[pnt * block->DIMS - N * block->traverse[i] +
                                  j * block->traverse[lnk] + lnk]
                           .phi);
                printf("-3 = %d, sum = %f\n",
                       pnt * block->DIMS - (j + 1) * block->traverse[i] + i,
                       block
                           ->link[pnt * block->DIMS -
                                  (j + 1) * block->traverse[i] + i]
                           .phi);
                printf("+1 = %d, sum = %f\n",
                       pnt * block->DIMS + N * block->traverse[lnk] +
                           j * block->traverse[i] + i,
                       block
                           ->link[pnt * block->DIMS + N * block->traverse[lnk] +
                                  j * block->traverse[i] + i]
                           .phi);
                printf("+2 = %d, sum = %f\n",
                       pnt * block->DIMS + N * block->traverse[i] -
                           j * block->traverse[lnk] + lnk,
                       block
                           ->link[pnt * block->DIMS + N * block->traverse[i] -
                                  j * block->traverse[lnk] + lnk]
                           .phi);
                printf(
                    "+3 = %d, sum = %f\n",
                    pnt * block->DIMS + j * block->traverse[i] + i,
                    block->link[pnt * block->DIMS + j * block->traverse[i] + i]
                        .phi);
#endif
            }
        }
    }
    return phi_sum;
}

// local action contibution
// This is based on all of the surrounding staples
float S(BLOCK *block, const int pnt, const int lnk, int next, int directional) {
#ifndef NXN
    float *phi_sum = staples(block, pnt, lnk);
#endif
#ifdef NXN
    float *phi_sum = nXn_staples(block, pnt, lnk, block->buf_size);
#endif
    float re_old = 0;
    float re_new = 0;
    int limit = block->DIMS;
    if (directional) {
        limit = lnk;
    }
    int i;
    for (i = 0; i < limit; i++) {
        float im;
        float re;
        if (i != lnk) {
            if (!directional) {
                phase_to_z(phi_sum[2 * i] +
                               block->link[pnt * block->DIMS + lnk].phi,
                           &re, &im);
                re_old += re;
            }
            phase_to_z(phi_sum[2 * i + 1] +
                           block->link[pnt * block->DIMS + lnk].phi,
                       &re, &im);
            re_old += re;
            if (next) {
                phase_to_z(phi_sum[2 * i] +
                               block->link[pnt * block->DIMS + lnk].next,
                           &re, &im);
                re_new += re;
                phase_to_z(phi_sum[2 * i + 1] +
                               block->link[pnt * block->DIMS + lnk].next,
                           &re, &im);
                re_new += re;
            }
        }
    }

    free(phi_sum);

    if (next) {
        return (re_old - re_new);
    }
    return -1.0 * block->beta * re_old;
}

// proposes a candidate updated phase and selects it according to a probability
float update(BLOCK *block, const int pnt, const int lnk, float rng) {
    float shift =
        2.0 * (block->eps) * ((float)rand() / (float)RAND_MAX) - block->eps;
    block->link[pnt * block->DIMS + lnk].next =
        block->link[pnt * block->DIMS + lnk].phi + shift;
    float dS = S(block, pnt, lnk, 1, 0);
    float prob = exp(-1.0 * block->beta * dS);

    if (rng < prob) {
        block->link[pnt * block->DIMS + lnk].phi =
            block->link[pnt * block->DIMS + lnk].next;
        phase_to_z(block->link[pnt * block->DIMS + lnk].phi,
                   &block->link[pnt * block->DIMS + lnk].re,
                   &block->link[pnt * block->DIMS + lnk].im);
    }
    return prob;
}

// total action contribution of the MPI block
float block_S(BLOCK *block) {
    float act = 0;
    int i;
    for (i = 0; i < block->points; i++) {
        if (!block->buf[i]) {
            int j;
            for (j = 1; j < block->DIMS; j++) {
                act += S(block, i, j, 0, 1);
            }
        }
    }
    return act;
}
