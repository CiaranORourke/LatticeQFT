#include "block.h"

void transient(BLOCK *block, const int skips);
float metropoloop(BLOCK *block, const int reps, const int multi);
void overrelax(BLOCK *block, const int reps);
float jackknife(float *x, const int N, const float mean);
void print_lattice_action(BLOCK *block);
void print_lattice_energy(BLOCK *block);
