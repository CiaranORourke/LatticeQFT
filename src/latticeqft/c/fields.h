#pragma once

typedef struct link {
    float phi; // [0, 2pi)
    float re;  // cos(phi)
    float im;  // sin(phi)

    float next;
} LINK;

typedef struct site {
    int spin; // -1 or +1

    int *dir; // stores indexes of nearest neighbours
} SITE;
