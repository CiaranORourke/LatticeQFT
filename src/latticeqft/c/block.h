#pragma once

#include "fields.h"

typedef struct block {
    float h;
    float J; // coupling constant
    float T;
    float B; // useful to reduce fp division
    float a; // lattice spacing
    float beta;

    float eps; // range of phi updates

    unsigned int DIMS;   // number of dimensions
    int *dim;            // array of dimension sizes
    unsigned int points; // total number of sites

    int *buf;     // wether site is a buffer region
    int buf_size; // number of buffer faces
    int *edge;    // -1 is not edge, 0-2D-1 numbers edge, 2d corner

    int *traverse; // stores movement through array that corresponds to movement
                   // through each dimension

    int *dir; // store rank of nearest neighbouring processes

    SITE *site;
    LINK *link;
} BLOCK;

void create_grid(BLOCK *block);
void free_grid(BLOCK *block);
void randomise_grid(BLOCK *block);
int choose_red_point(BLOCK *block);
int choose_black_point(BLOCK *block);
