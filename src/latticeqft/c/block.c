#include "latticeqft/latticeqft.h"

#include "block.h"
#include "ranlxs.h"
#include <math.h>
#include <stdlib.h>

// allocate memory for grid
// determines if points are buffers
// this ise used to ensure buffer regions do not contribute to calculations
// determines if points are edges
// this assists non-blocking communications
void create_grid(BLOCK *block) {
    // determine MPI task region sizes
    OK_SUB_LATTICE_DIMENSIONS =
        1; // checkerboard scheme needs even-sided sublattices
    int j;
    for (j = 0; j < block->DIMS; j++) {
        // note +2's for buffer region on either side
        block->dim[j] = DIM[j] / MPI_DIM[j] + 2 * block->buf_size;
        if (X[j] < DIM[j] % MPI_DIM[j]) // evenly distribute remainder
        {
            block->dim[j]++;
        }
        if (block->dim[j] % 2) {
            OK_SUB_LATTICE_DIMENSIONS = 0;
        }
    }
    block->points = 1;
    unsigned int i;
    for (i = 0; i < block->DIMS; i++) {
        block->points *= block->dim[i]; // determine total number of points
    }

    // There are DIMS links for every site
    // These are stored contiguously as if we introduce a new lower order
    // dimension with size DIMS
    // This multiplier multiplies memory allocations in the gauge case

    if (ISING) {
        block->site = malloc(block->points * sizeof(SITE));
    }
    if (GAUGE) {
        block->link = malloc(block->DIMS * block->points * sizeof(LINK));
    }
    block->traverse = malloc(block->DIMS * sizeof(int));
    // used to move through subsequent dimensions
    // i.e to increment in a dimension we must
    // skip all the points of the 'lower' dimensions
    // accounts for buffer regions
    int offset = 1;
    for (j = 0; j < block->DIMS; j++) {
        block->traverse[j] = offset;
        offset *= block->dim[j];
    }
    block->buf = malloc(block->points * sizeof(int));
    block->edge = malloc(block->points * sizeof(int));
    // determine indices of nearest neighbours
    // determine buffer sites
    for (i = 0; i < block->points; i++) {
        block->buf[i] = 0;
        block->edge[i] = -1;
        if (ISING) {
            block->site[i].dir = malloc(2 * block->DIMS * sizeof(int));
        }
        // first two fields store nearest neighbours in first dimension
        // direction of increasing index (eg. left is stored before right)
        // next two store nearest neighbours in second dimension, etc

        int j;
        for (j = 0; j < block->DIMS; j++) {
            if (ISING) {
                block->site[i].dir[2 * j + 1] = i + block->traverse[j];
            }
            if (((i + block->buf_size * block->traverse[j]) %
                 (block->traverse[j] * block->dim[j])) <
                block->buf_size *
                    block->traverse[j]) // positive boundary condition
            {
                block->buf[i] = 1; // site is in buffer region
            }
            if (ISING) {
                block->site[i].dir[2 * j] = i - block->traverse[j];
            }
            if ((i % (block->traverse[j] * block->dim[j])) <
                block->buf_size *
                    block->traverse[j]) // negative boundary condition
            {
                block->buf[i] = 1;
            }
        }
        if (!block->buf[i]) // check if edge. Basically reuse boundary condition
                            // and check if point is next to it
        {
#ifdef buf_debug
            int rank;
            if (1) //! rank)
            {
                printf("%d is apparently not a buffer\n", i);
            }
#endif
            for (j = 0; j < block->DIMS; j++) {

                if (((i + 2 * block->buf_size * block->traverse[j]) %
                     (block->traverse[j] * block->dim[j])) <
                    block->buf_size * block->traverse[j]) // edge
                {
#ifdef buf_debug
                    if (1) //! rank)
                    {
                        printf("%d is apparently an edge\n", i);
                    }
#endif
                    if (block->edge[i] > -1) // corner piece
                    {
                        block->edge[i] = 2 * block->DIMS;
                    } else {
                        block->edge[i] = 2 * j + 1;
                    }
                }
                if (((i - block->buf_size * block->traverse[j]) %
                     (block->traverse[j] * block->dim[j])) <
                    block->buf_size * block->traverse[j]) // edge of block
                {
#ifdef buf_debug
                    if (1) //! rank)
                    {
                        printf("%d is apparently an edge\n", i);
                    }
#endif
                    if (block->edge[i] > -1) // corner piece
                    {
                        block->edge[i] = 2 * block->DIMS;
                    } else {
                        block->edge[i] = 2 * j;
                    }
                }
            }
        }
        if (ISING && block->buf[i]) {
            free(block->site[i].dir); // would lose pointer to this memory upon
                                      // receiving in Halo exchange
        }
    }
    if (GAUGE) {
        int j;
        for (j = 0; j < block->DIMS; j++) {
            block->traverse[j] *=
                block->DIMS; // account for multiple links at sites
        }
    }
}
// randomly choose spin/phase at each site/link
void randomise_grid(BLOCK *block) {
    int mul = 1;
    if (GAUGE) {
        mul = block->DIMS;
    }
    float *U = malloc(mul * block->points * sizeof(float));
    ranlxs(U, mul * block->points);
    unsigned int i;
    for (i = 0; i < block->points; i++) {
        if (ISING) {
            block->site[i].spin = 1;
            if (U[i] < 0.5) // each point may be +/- 1 with equal probability
            {
                block->site[i].spin -= 2;
            }
        }
        if (GAUGE) {
            int j;
            for (j = 0; j < block->DIMS; j++) {
                block->link[i * block->DIMS + j].phi =
                    2.0 * M_PI * U[i * block->DIMS + j]; // [0, 2π)
                block->link[i * block->DIMS + j].next = 0;
                block->link[i * block->DIMS + j].re = 0;
                block->link[i * block->DIMS + j].im = 0;
            }
        }
    }
    free(U);
}

// Following two functions are used for the checkerboard scheme

// returns the index of a random 'red' point of the block
int choose_red_point(BLOCK *block) {
    // define red points as those having an even traversal sum
    // we ignore moving in from the buffer for this
    // need only fix this condition on the last traversal
    int odd = 0;
    int pt = 0;
    int mv;
    int i;
    for (i = 0; i < block->DIMS - 1; i++) {
        mv = block->buf_size + rand() % (block->dim[i] - 2 * block->buf_size);
        pt += mv * block->traverse[i];
        odd += mv - 1; // ignore buffer
    }
    odd = odd % 2;
    if (!odd) // we are already on an even point, so traverse even number
    {
        mv = block->buf_size +
             2 * (rand() % ((block->dim[i] - 2 * block->buf_size) / 2));
    } else // we are on an odd point, so traverse odd number
    {
        mv = block->buf_size + 1 +
             2 * (rand() % ((block->dim[i] - 2 * block->buf_size) / 2));
    }
    pt += mv * block->traverse[i];
    return pt;
}

// returns the index of a random 'black' point of the block
int choose_black_point(BLOCK *block) {
    // define black points as those having an odd traversal sum
    // we ignore moving in from the buffer for this
    // need only fix this condition on the last traversal
    int odd = 0;
    int pt = 0;
    int mv;
    int i;
    for (i = 0; i < block->DIMS - 1; i++) {
        mv = block->buf_size + rand() % (block->dim[i] - 2 * block->buf_size);
        pt += mv * block->traverse[i];
        odd += mv - 1; // ignore buffer
    }
    odd = odd % 2;
    if (odd) // we are already on an odd point, so traverse even number
    {
        mv = block->buf_size +
             2 * (rand() % ((block->dim[i] - 2 * block->buf_size) / 2));
    } else // we are on an even point, so traverse odd number
    {
        mv = block->buf_size + 1 +
             2 * (rand() % ((block->dim[i] - 2 * block->buf_size) / 2));
    }
    pt += mv * block->traverse[i];
    return pt;
}

// cleanup sublattice
void free_grid(BLOCK *block) {
    if (ISING) {
        int i;
        for (i = 0; i < block->points; i++) {
            if (!block->buf[i]) {
                free(block->site[i].dir);
            }
        }
        free(block->site);
    }
    if (GAUGE) {
        free(block->link);
    }

    free(block->dir);
    free(block->buf);
    free(block->edge);
    free(block->dim);
    free(block->traverse);
    free(block);
}
