#include "block.h"

void run_ising_temperatureRange(BLOCK *block, const int MCsweeps,
                                const int metroSkips, const int transkips,
                                float T1, float T2, float dT, char *filename);
void run_gauge_actionVtime(BLOCK *block, const int T, const int metro,
                           const int relax, const int multi);
