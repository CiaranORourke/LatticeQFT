#include "comms.h"
#include <stdio.h>
#include <stdlib.h>

// Set up Cartesian topology of processors
void create_topology(BLOCK *block) {
    MPI_Dims_create(size, block->DIMS, MPI_DIM);
    int pbc[block->DIMS];
    int i;
    for (i = 0; i < block->DIMS; i++) {
        pbc[i] = 1;
    }
    MPI_Cart_create(MPI_COMM_WORLD, block->DIMS, MPI_DIM, pbc, 1, &cartcomm);
    MPI_Comm_rank(cartcomm, &rank);
    X = malloc(block->DIMS * sizeof(int));
    MPI_Cart_coords(cartcomm, rank, block->DIMS, X);

    // determine nearest neighbouring tasks
    block->dir = malloc(2 * block->DIMS * sizeof(int));
    for (i = 0; i < block->DIMS; i++) {
        MPI_Cart_shift(cartcomm, i, 1, &(block->dir[2 * i]),
                       &(block->dir[2 * i + 1]));
    }
}

// Create a communications request for each nearest neighbour pair of processors
void set_up_persistent_comms(BLOCK *block) {
    int i;
    // each dimension has two faces
    for (i = 0; i < block->DIMS; i++) {
        if (ISING) {
            // send in negative direction, receive from positive direction
            MPI_Send_init(&(block->site[block->buf_size * block->traverse[i]]),
                          1, MPI_FACE[i], block->dir[2 * i], 0, cartcomm,
                          &send_req[2 * i]);
            MPI_Recv_init(&(block->site[(block->dim[i] - block->buf_size) *
                                        (block->traverse[i])]),
                          1, MPI_FACE[i], block->dir[2 * i + 1], MPI_ANY_TAG,
                          cartcomm, &recv_req[2 * i + 1]);

            // send in positive direction, receive from negative direciton
            MPI_Send_init(&(block->site[(block->dim[i] - 2 * block->buf_size) *
                                        (block->traverse[i])]),
                          1, MPI_FACE[i], block->dir[2 * i + 1], 0, cartcomm,
                          &send_req[2 * i + 1]);
            MPI_Recv_init(&(block->site[0]), 1, MPI_FACE[i], block->dir[2 * i],
                          MPI_ANY_TAG, cartcomm, &recv_req[2 * i]);
        }
        if (GAUGE) {
            // send in negative direction, receive from positive direction
            MPI_Send_init(&(block->link[block->buf_size * block->traverse[i]]),
                          1, MPI_FACE[i], block->dir[2 * i], 0, cartcomm,
                          &send_req[2 * i]);
            MPI_Recv_init(&(block->link[(block->dim[i] - block->buf_size) *
                                        (block->traverse[i])]),
                          1, MPI_FACE[i], block->dir[2 * i + 1], MPI_ANY_TAG,
                          cartcomm, &recv_req[2 * i + 1]);

            // send in positive direction, receive from negative direciton
            MPI_Send_init(&(block->link[(block->dim[i] - 2 * block->buf_size) *
                                        (block->traverse[i])]),
                          1, MPI_FACE[i], block->dir[2 * i + 1], 0, cartcomm,
                          &send_req[2 * i + 1]);
            MPI_Recv_init(&(block->link[0]), 1, MPI_FACE[i], block->dir[2 * i],
                          MPI_ANY_TAG, cartcomm, &recv_req[2 * i]);
        }
    }
    // persistent_red_to_black(block);
    // persistent_black_to_red(block);
}

// Set up capability of sending whole faces
void set_up_comms(BLOCK *block) {
    // set up datatype to send sites
    // NOTE: nearest neighbour index array is not needed for buffer regions
    // so it doesnt matter that the array is not transferred

    // MPI_Aint exti;
    // MPI_Type_extent(MPI_INT, &exti);
    if (ISING) {
        MPI_Datatype Proto_MPI_FIELD;
        MPI_Datatype type[2] = {MPI_INT, MPI_INT};

        int blocks[2] = {1, 1};
        MPI_Aint offsets[2];
        offsets[0] = offsetof(SITE, spin);
        offsets[1] = offsetof(SITE, dir);

        MPI_Type_create_struct(2, blocks, offsets, type, &Proto_MPI_FIELD);
        MPI_Type_commit(&Proto_MPI_FIELD);

        // must account for padding of struct
        MPI_Type_create_resized(Proto_MPI_FIELD, offsets[0],
                                (MPI_Aint)sizeof(SITE), &MPI_FIELD);

        MPI_Type_free(&Proto_MPI_FIELD);
        MPI_Type_commit(&MPI_FIELD);
    }
    if (GAUGE) {
        MPI_Datatype Proto_MPI_LINK;
        MPI_Datatype type[4] = {MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};

        int blocks[4] = {1, 1, 1, 1};
        MPI_Aint offsets[4];
        offsets[0] = offsetof(LINK, phi);
        offsets[1] = offsetof(LINK, re);
        offsets[2] = offsetof(LINK, im);
        offsets[3] = offsetof(LINK, next);

        MPI_Type_create_struct(4, blocks, offsets, type, &Proto_MPI_LINK);
        MPI_Type_commit(&Proto_MPI_LINK);
        MPI_Aint exti;
        MPI_Aint lb;
        MPI_Type_get_extent(Proto_MPI_LINK, &lb, &exti);

        // resize for padding
        MPI_Type_create_resized(Proto_MPI_LINK, offsets[0],
                                (MPI_Aint)sizeof(LINK), &MPI_FIELD);

        MPI_Type_free(&Proto_MPI_LINK);
        MPI_Type_commit(&MPI_FIELD);
    }
    MPI_Aint lb;
    MPI_Type_get_extent(MPI_FIELD, &lb, &extFIELD);
    /*
    int type_size;
    MPI_Type_size(MPI_FIELD, &type_size);
    MPI_Aint ext;
    MPI_Type_get_extent(MPI_FIELD, &ext);
    if(!rank)
    {
            printf("field size = %d\n", type_size);
            printf("field extent = %d\n", ext);
    }
    */

    // boundary data_types
    // There is an orientation of slice through the lattice for each dimension
    // For each dimension there are four faces we need to identify;
    // negative direction buffer face, negative direction edge face
    // positive "                    , positive "
    // Only No. dims distinct datatypes are required.
    // With different starting indexes a datatype can realise each of the four
    // faces

    // First create datatypes
    MPI_FACE = malloc(block->DIMS * sizeof(MPI_Datatype));

    int link_dim = 0;
    if (GAUGE) {
        link_dim = 1; // will treat links as an extra dimension in halo exchange
    }

    int dimensions[block->DIMS + link_dim];
    int sizes[block->DIMS + link_dim];
    int starts[block->DIMS + link_dim];
    // if ising, these will be overwritten anyway
    starts[0] = 0;
    sizes[0] = block->DIMS; // links per site, lowest order dimension
    dimensions[0] = block->DIMS;

    int i;
    for (i = link_dim; i < block->DIMS + link_dim; i++) {
        starts[i] = 0;
        sizes[i] = block->dim[i - link_dim];
        dimensions[i] = block->dim[i - link_dim];
    }

    // The Ising and Gauge cases work in much the same way except that for for
    // each Site that would be packed in a FACE struct in the Ising case,
    // block->DIMS Links are in the gauge case
    for (i = link_dim; i < block->DIMS + link_dim; i++) {
        sizes[i] = block->buf_size; // don't traverse this dimension, only take
                                    // number of buffer slices

        MPI_Type_create_subarray(block->DIMS + link_dim, dimensions, sizes,
                                 starts, MPI_ORDER_FORTRAN, MPI_FIELD,
                                 &MPI_FACE[i - link_dim]);
        MPI_Type_commit(&MPI_FACE[i - link_dim]);

        sizes[i] =
            block->dim[i -
                       link_dim]; // reset this dimension for traversing fully
    }
#if defined(PERSISTENT) || defined(NONBLOCKING)
    send_req = (MPI_Request *)malloc(2 * block->DIMS * sizeof(MPI_Request));
    recv_req = (MPI_Request *)malloc(2 * block->DIMS * sizeof(MPI_Request));

    for (i = 0; i < 2 * block->DIMS; i++) {
        send_req[i] = MPI_REQUEST_NULL;
        recv_req[i] = MPI_REQUEST_NULL;
    }

    set_up_persistent_comms(block);
#endif

#ifdef BLOCKING
    transfer_all_faces(block);
#endif
#ifdef NONBLOCKING
    non_block_transfer_all(block);
#endif
#ifdef PERSISTENT
    persistent_transfer_all(block);
#endif
}

// Non-blocking - Make all buffer regions up-to-date
void transfer_all_faces(BLOCK *block) {
    int i;
    // each dimension has two faces
    for (i = 0; i < block->DIMS; i++) {
        if (ISING) {
            // send in negative direction, receive from positive direction
            MPI_Sendrecv(&(block->site[block->buf_size * block->traverse[i]]),
                         1, MPI_FACE[i], block->dir[2 * i], 0,
                         &(block->site[(block->dim[i] - block->buf_size) *
                                       (block->traverse[i])]),
                         1, MPI_FACE[i], block->dir[2 * i + 1], MPI_ANY_TAG,
                         cartcomm, MPI_STATUS_IGNORE);

            // send in positive direction, receive from negative direciton
            MPI_Sendrecv(&(block->site[(block->dim[i] - 2 * block->buf_size) *
                                       (block->traverse[i])]),
                         1, MPI_FACE[i], block->dir[2 * i + 1], 0,
                         &(block->site[0]), 1, MPI_FACE[i], block->dir[2 * i],
                         MPI_ANY_TAG, cartcomm, MPI_STATUS_IGNORE);
        }
        if (GAUGE) {
            MPI_Sendrecv(&(block->link[block->buf_size * block->traverse[i]]),
                         1, MPI_FACE[i], block->dir[2 * i], 0,
                         &(block->link[(block->dim[i] - block->buf_size) *
                                       (block->traverse[i])]),
                         1, MPI_FACE[i], block->dir[2 * i + 1], MPI_ANY_TAG,
                         cartcomm, MPI_STATUS_IGNORE);
            MPI_Sendrecv(&(block->link[(block->dim[i] - 2 * block->buf_size) *
                                       (block->traverse[i])]),
                         1, MPI_FACE[i], block->dir[2 * i + 1], 0,
                         &(block->link[0]), 1, MPI_FACE[i], block->dir[2 * i],
                         MPI_ANY_TAG, cartcomm, MPI_STATUS_IGNORE);
        }
    }
}

// non-blocking version
// matching MPI_Wait call in algorithms where appropriate
void non_block_transfer_all(BLOCK *block) {
    int i;
    for (i = 0; i < block->DIMS; i++) {
        if (ISING) {
            // send in negative direction
            MPI_Isend(&(block->site[block->buf_size * block->traverse[i]]), 1,
                      MPI_FACE[i], block->dir[2 * i], 0, cartcomm,
                      &send_req[2 * i]);
            // receive from positive direction
            MPI_Irecv(&(block->site[(block->dim[i] - block->buf_size) *
                                    (block->traverse[i])]),
                      1, MPI_FACE[i], block->dir[2 * i + 1], MPI_ANY_TAG,
                      cartcomm, &recv_req[2 * i + 1]);
            // send in  positive direction
            MPI_Isend(&(block->site[(block->dim[i] - 2 * block->buf_size) *
                                    (block->traverse[i])]),
                      1, MPI_FACE[i], block->dir[2 * i + 1], 0, cartcomm,
                      &send_req[2 * i + 1]);
            // receive from negative direction
            MPI_Irecv(&(block->site[0]), 1, MPI_FACE[i], block->dir[2 * i],
                      MPI_ANY_TAG, cartcomm, &recv_req[2 * i]);
        }
        if (GAUGE) {
            MPI_Isend(&(block->link[block->buf_size * block->traverse[i]]), 1,
                      MPI_FACE[i], block->dir[2 * i], 0, cartcomm,
                      &send_req[2 * i]);
            MPI_Irecv(&(block->link[(block->dim[i] - block->buf_size) *
                                    (block->traverse[i])]),
                      1, MPI_FACE[i], block->dir[2 * i + 1], MPI_ANY_TAG,
                      cartcomm, &recv_req[2 * i + 1]);
            MPI_Isend(&(block->link[(block->dim[i] - 2 * block->buf_size) *
                                    (block->traverse[i])]),
                      1, MPI_FACE[i], block->dir[2 * i + 1], 0, cartcomm,
                      &send_req[2 * i + 1]);
            MPI_Irecv(&(block->link[0]), 1, MPI_FACE[i], block->dir[2 * i],
                      MPI_ANY_TAG, cartcomm, &recv_req[2 * i]);
        }
    }
}

// same logic as non-blocking
void persistent_transfer_all(BLOCK *block) {
    int i;
    for (i = 0; i < block->DIMS; i++) {
        MPI_Start(&send_req[2 * i]);
        MPI_Start(&recv_req[2 * i + 1]);
        MPI_Start(&send_req[2 * i + 1]);
        MPI_Start(&recv_req[2 * i]);
    }
}

// functions similarly to MPI_Gather except that received data is ordered by
// index then rank rather than rank then index
void MPI_Gather_interleaved(void *sendbuf, int sendcnt, MPI_Datatype sendtype,
                            void *recvbuf, int recvcnt, MPI_Datatype recvtype,
                            int root, MPI_Comm comm) {
    MPI_Request r1;
    MPI_Isend(sendbuf, sendcnt, sendtype, root, 0, comm, &r1);
    if (rank == root) {
        MPI_Request r2[size];
        MPI_Status s[size];
        MPI_Datatype vec;
        MPI_Type_vector(recvcnt, 1, size, recvtype, &vec);
        MPI_Type_commit(&vec);

        int stype_size;
        MPI_Type_size(recvtype, &stype_size);
        int i;
        for (i = 0; i < size; i++) {
            MPI_Irecv(recvbuf + i * stype_size, 1, vec, i, MPI_ANY_TAG, comm,
                      &r2[i]);
        }
        MPI_Type_free(&vec);
        MPI_Waitall(size, r2, s);
    }
    MPI_Wait(&r1, MPI_STATUS_IGNORE);
}

void print_grid(BLOCK *block) {
#ifdef NONBLOCKING
// MPI_Waitall(2*block->DIMS, req, MPI_STATUSES_IGNORE);
#endif
#ifdef PERSISTENT
    MPI_Waitall(2 * block->DIMS, send_req, MPI_STATUSES_IGNORE);
    MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
#endif
    int j;
    for (j = 0; j < size; j++) {
        if (rank == j) {
            printf("(%d,%d)\n", X[0], X[1]);
            int i;
            for (i = 0; i < block->points; i++) {
                printf("%d\t", block->site[i].spin);
                if ((i + 1) % block->dim[0] == 0) {
                    printf("\n");
                }
            }
            printf("\n");
        }
        MPI_Barrier(cartcomm);
        fflush(stdout);
        usleep(200);
    }
}

// clean up all MPI related things that MPI will allow
void free_mpi_data(int D) {
    free(X);
    free(MPI_DIM);
    MPI_Comm_free(&cartcomm);
    MPI_Type_free(&MPI_FIELD);
    int i;
    for (i = 0; i < D; i++) {
        MPI_Type_free(&MPI_FACE[i]);
#ifdef PERSISTENT
        MPI_Request_free(&(send_req[2 * i]));
        MPI_Request_free(&(send_req[2 * i + 1]));
        MPI_Request_free(&(recv_req[2 * i]));
        MPI_Request_free(&(recv_req[2 * i + 1]));
#endif
    }
#if defined(NONBLOCKING) || defined(PERSISTENT)
    free(send_req);
    free(recv_req);
#endif
    free(MPI_FACE);
    free(DIM);
}
