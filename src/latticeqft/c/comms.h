#include "block.h"
#include "latticeqft/latticeqft.h"
#include <mpi.h>
#include <unistd.h>

#define PERSISTENT

// mpi constants

MPI_Comm cartcomm;

MPI_Datatype *MPI_FACE; // Datatypes for communication boundaries
MPI_Datatype MPI_FIELD; // For sending field structs
MPI_Aint extFIELD;

// used for non-blocking/persistent comm Halo exchange
MPI_Request *send_req;
MPI_Request *recv_req;

void create_topology(BLOCK *block);
void set_up_persistent_comms(BLOCK *block);
void set_up_comms(BLOCK *block);
void transfer_all_faces(BLOCK *block);
void non_block_transfer_all(BLOCK *block);
void set_up_persistent_comms(BLOCK *block);
void persistent_transfer_all(BLOCK *block);
void MPI_Gather_interleaved(void *sendbuf, int sendcnt, MPI_Datatype sendtype,
                            void *recvbuf, int recvcnt, MPI_Datatype recvtype,
                            int root, MPI_Comm comm);
void free_mpi_data(int D);

void print_grid(BLOCK *block);
