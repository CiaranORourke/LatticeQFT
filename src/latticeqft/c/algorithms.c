#include "algorithms.h"
#include "comms.h"
#include "gauge.h"
#include "ising.h"
#include "ranlxs.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// function to reach thermal equilibrium
void transient(BLOCK *block, const int skips) { metropoloop(block, skips, 1); }

// performs one metropolis loop
// that is, lattice points number of potential updates
// reps is used to avoid multiple calls to function when no samples are needed
// reps = 1 is a regular metropolis update
// multi allows for multi-hit Metropolis algorithm - gauge only
// returns acceptance rate
float metropoloop(BLOCK *block, const int reps, const int multi) {
    float prob = 0.0;
    int mul = 1;
    int link_mul = 1;
    if (GAUGE) {
        mul = block->DIMS;
        link_mul = 2;
    }
    // red-black update pattern preserves detailed balance by avoiding updating
    // neighbouring points that are on the edge of neighbouring blocks at the
    // same time
    int *rb_pick = malloc(link_mul * reps * sizeof(int));
    if (!rank) {
        int i;
        for (i = 0; i < reps; i++) {
            if (ISING) {
                rb_pick[i] = rand() % 2;
            }
            if (GAUGE) {
                rb_pick[i] = rand() % 2;
                rb_pick[reps + i] =
                    rand() % block->DIMS; // updates must be on same links
            }
        }
    }
    // This way a proc knows what it may update next at all times
    MPI_Bcast(rb_pick, link_mul * reps, MPI_INT, 0, cartcomm);

    int k;
    for (k = 0; k < reps; k++) {
        float *U = malloc(mul * block->points * sizeof(float));
        ranlxs(U, mul * block->points);
        int i;
        for (i = 0; i < mul * block->points; i++) {
            int point;
            if (rb_pick[k]) {
                point = choose_black_point(block);
            } else {
                point = choose_red_point(block);
            }
            int pt = point;
            int lk;
            if (GAUGE) {
                pt = point / block->DIMS;
                lk = rb_pick[reps + k];
            }
#ifdef NONBLOCKING
            if (block->edge[pt] > -1) {
                if (block->edge[pt] == 2 * block->DIMS) // corner
                {
                    MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
                } else // only need one communication to finish
                {
                    MPI_Wait(&recv_req[block->edge[pt]], MPI_STATUS_IGNORE);
                }
            }
#endif

#ifdef PERSISTENT
            if (block->edge[pt] > -1) {
                if (block->edge[pt] == 2 * block->DIMS) // corner
                {
                    // MPI_Waitall(2*block->DIMS, send_req,
                    // MPI_STATUSES_IGNORE);
                    MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
                } else // only need one communication to finish
                {
                    // MPI_Wait(&send_req[block->edge[pt]],
                    // MPI_STATUSES_IGNORE);
                    MPI_Wait(&recv_req[block->edge[pt]], MPI_STATUSES_IGNORE);
                }
            }
#endif
            if (ISING) {
                prob += flip_or_not(block, pt, U[i]);
            }
            if (GAUGE) {
                int k;
                for (k = 0; k < multi; k++) {
                    prob += update(block, pt, lk, U[i]);
                }
            }
        }
        free(U);

#ifdef BLOCKING
        transfer_all_faces(block);
#endif
#ifdef NONBLOCKING
        // MPI_Waitall(2*block->DIMS, send_req, MPI_STATUSES_IGNORE);
        MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
        non_block_transfer_all(block);
#endif
#ifdef PERSISTENT
        // MPI_Waitall(2*block->DIMS, send_req, MPI_STATUSES_IGNORE);
        MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
        persistent_transfer_all(block);
#endif
    }
    free(rb_pick);
    return prob / ((float)reps * mul * block->points * multi);
}

// execute reps overrelaxation sweeps of the lattice
void overrelax(BLOCK *block, const int reps) {
    int *rb_pick = malloc(2 * reps * sizeof(int));
    if (!rank) {
        int i;
        for (i = 0; i < reps; i++) {
            rb_pick[i] = rand() % 2;
            rb_pick[reps + i] = rand() % block->DIMS;
        }
    }
    MPI_Bcast(rb_pick, 2 * reps, MPI_INT, 0, cartcomm);

    int k;
    for (k = 0; k < reps; k++) {
        int i;
        for (i = 0; i < block->DIMS * block->points; i++) {
            int point = rb_pick[reps + k]; // choose link orientation
            if (rb_pick[k]) {
                point += choose_red_point(block);
            } else {
                point += choose_black_point(block);
            }
            int pt = point / block->DIMS;
            int lk = point % block->DIMS;
#ifdef NONBLOCKING
            if (block->edge[pt] > -1) {
                if (block->edge[pt] == 2 * block->DIMS) // corner
                {
                    MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
                    // MPI_Waitall(2*block->DIMS, send_req,
                    // MPI_STATUSES_IGNORE);
                } else // only need one communication to finish
                {
                    MPI_Wait(&recv_req[block->edge[pt]], MPI_STATUSES_IGNORE);
                    // MPI_Wait(&send_req[block->edge[pt]],
                    // MPI_STATUSES_IGNORE);
                }
            }
#endif
#ifdef PERSISTENT
            if (block->edge[pt] > -1) {
                if (block->edge[pt] == 2 * block->DIMS) // corner
                {
                    // MPI_Waitall(2*block->DIMS, send_req,
                    // MPI_STATUSES_IGNORE);
                    MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
                } else // only need one communication to finish
                {
                    // MPI_Wait(&send_req[block->edge[pt]],
                    // MPI_STATUSES_IGNORE);
                    MPI_Wait(&recv_req[block->edge[pt]], MPI_STATUSES_IGNORE);
                }
            }
#endif
#ifndef NXN
            float *phi_sum = staples(block, pt, lk);
#endif
#ifdef NXN
            float *phi_sum = nXn_staples(block, pt, lk, block->buf_size);
#endif
            float staple_re_sum = 0;
            float staple_im_sum = 0;
            int l;
            for (l = 0; l < block->DIMS; l++) {
                float re;
                float im;
                if (l != lk) {
                    phase_to_z(phi_sum[2 * l], &re, &im);
                    staple_re_sum += re;
                    staple_im_sum += im;
                    phase_to_z(phi_sum[2 * l + 1], &re, &im);
                    staple_re_sum += re;
                    staple_im_sum += im;
                }
            }
            free(phi_sum);
            float alpha;
            z_to_phase(&alpha, staple_re_sum, staple_im_sum);
            block->link[point].phi =
                2.0 * M_PI - 2.0 * alpha - block->link[point].phi; // update
        }
#ifdef BLOCKING
        transfer_all_faces(block);
#endif
#ifdef NONBLOCKING
        // MPI_Waitall(2*block->DIMS, send_req, MPI_STATUSES_IGNORE);
        MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
        non_block_transfer_all(block);
#endif
#ifdef PERSISTENT
        // MPI_Waitall(2*block->DIMS, send_req, MPI_STATUSES_IGNORE);
        MPI_Waitall(2 * block->DIMS, recv_req, MPI_STATUSES_IGNORE);
        persistent_transfer_all(block);
#endif
    }
    free(rb_pick);
}

// jackknife estimation of variance
float jackknife(float *x, const int N, const float mean) {
    float *means = calloc(N, sizeof(float)); // initialise to 0
    float var = 0.0;
    int i;
    for (i = 0; i < N; i++) {
        int j = 0;
        while (j < i) {
            means[i] += x[j];
            j++;
        }
        j++; // skip i
        while (j < N) {
            means[i] += x[j];
            j++;
        }
        means[i] /= (float)(N - 1);
        var += (mean - means[i]) * (mean - means[i]);
    }
    free(means);
    var *= (float)(N - 1);
    var /= (float)N;
    return var;
}

void print_lattice_action(BLOCK *block) {
    float s = block_S(block);
    float S;
    MPI_Reduce(&s, &S, 1, MPI_FLOAT, MPI_SUM, 0, cartcomm);
    if (!rank) {
        printf("Lattice Action = %f\n", S);
    }
}

void print_lattice_energy(BLOCK *block) {
    float e = block_energy(block);
    float E;
    MPI_Reduce(&e, &E, 1, MPI_FLOAT, MPI_SUM, 0, cartcomm);
    if (!rank) {
        printf("Lattice Energy = %f\n", E / (float)2);
    }
}
