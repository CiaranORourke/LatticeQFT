#include <memory>
#include <vector>

#include <latticeqft/field/field.h>

namespace latticeqft {
namespace sublattice {

template <size_t Rank, typename T, typename U = T> class Sublattice {

  public:
    static constexpr const size_t m_rank = Rank;

  private:
    std::vector<int> m_dimension_sizes;

    unsigned int m_buffer_width = 1;
    field::Nd_vector<bool, m_rank> m_is_buffer;

    /*
     * number of boundaries a vertex lies on
     */
    field::Nd_vector<int, m_rank> m_num_boundaries;

    std::vector<int> m_neighbour;

    const bool m_vertices_defined = false;
    const bool m_edges_defined = false;

    void setup_fields();
    void initialise_fields();

  protected:
    std::unique_ptr<field::Vertices<T, m_rank>> m_vertices;
    std::unique_ptr<field::Edges<U, m_rank>> m_edges;

  public:
    Sublattice(std::unique_ptr<field::Vertices<T, Rank>> vertices,
               std::vector<int> dimension_sizes)
        : m_vertices(std::move(vertices)), m_dimension_sizes(dimension_sizes),
          m_vertices_defined(true) {
        setup_fields();
        initialise_fields();
    }

    Sublattice(std::unique_ptr<field::Edges<U, Rank>> edges,
               std::vector<int> dimension_sizes)
        : m_edges(std::move(edges)), m_dimension_sizes(dimension_sizes),
          m_edges_defined(true) {
        setup_fields();
        initialise_fields();
    }

    Sublattice(std::unique_ptr<field::Vertices<T, Rank>> vertices,
               std::unique_ptr<field::Edges<U, Rank>> edges,
               std::vector<int> dimension_sizes)
        : m_vertices(std::move(vertices)), m_edges(std::move(edges)),
          m_dimension_sizes(dimension_sizes), m_vertices_defined(true),
          m_edges_defined(true) {
        setup_fields();
        initialise_fields();
    }

    std::vector<int> dimension_sizes() const { return m_dimension_sizes; }
    int dimension_size(int x) const { return m_dimension_sizes[x]; }

    bool is_buffer(const int x) const { return m_is_buffer[x]; }
    int num_boundaries(int x) const { return m_num_boundaries[x]; }
};

template <size_t Rank, typename T, typename U>
void Sublattice<Rank, T, U>::setup_fields() {
    /* initialise Vertices and/or Edges with appropriate dimensions */
    std::vector<int> field_dimension_sizes = m_dimension_sizes;
    for (int i = 0; i < m_rank; i++) {
        /* +2 for halo regions */
        field_dimension_sizes[i] += 2 * m_buffer_width;
    }

    if (m_vertices_defined) {
        m_vertices->resize(field_dimension_sizes);
    }
    if (m_edges_defined) {
        m_edges->resize(field_dimension_sizes);
    }

    /* determine buffer regions and number of boundaries each vertix lies on */
    m_is_buffer = field::Nd_vector<bool, m_rank>(field_dimension_sizes);
    m_num_boundaries = field::Nd_vector<int, m_rank>(field_dimension_sizes);
    /* TODO */
}

template <size_t Rank, typename T, typename U>
void Sublattice<Rank, T, U>::initialise_fields() {
    if (m_vertices_defined) {
        m_vertices->initialise();
    }
    if (m_edges_defined) {
        m_edges->initialise();
    }
}

} // namespace sublattice
} // namespace latticeqft
