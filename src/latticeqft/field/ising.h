#include "field.h"

/* TODO temp */
#include <cmath>
#include <cstdlib>
#include <iostream>

namespace latticeqft {
namespace field {

typedef struct ising_variable {
    int spin;
} Ising_variable;

/* Ising model implementation */
template <size_t Rank> class Ising : public Vertices<Ising_variable, Rank> {
  public:
    static constexpr const size_t m_rank = Rank;

  private:
    const float h = 0.0;
    const float J = 1.0;
    const float T = 0.1;
    const float B = 1.0 / T;

    float local_hamiltonian(const size_t &index) const;
    float local_update_energy_change(const size_t &index) const;

  public:
    Ising() {}

    template <typename X>
    Ising(const std::array<X, m_rank> &sizes)
        : Vertices<Ising_variable, m_rank>(sizes) {}

    template <typename X>
    Ising(const std::vector<X> &sizes)
        : Vertices<Ising_variable, m_rank>(sizes) {}

    void initialise() override;
    void update() override;

    void hamiltonian(int dim, std::vector<int> &index, float &H) const;
    float hamiltonian() const;
    float magnetisation() const;

    void print(int dim, std::vector<int>& index) const;
    void print() const;
};

template <size_t Rank> void Ising<Rank>::initialise() {
    for (int i = 0; i < this->size(); i++) {
        this->data(i).spin = 2 * (rand() % 2) - 1;
    }
}

template <size_t Rank>
float Ising<Rank>::local_hamiltonian(const size_t &index) const {
    float neighbour_spins_sum = 0;

    /* sum over neighbouring spins */
    for (int i = 0; i < m_rank; i++) {
        neighbour_spins_sum += this->data(index + this->stride(i)).spin +
                               this->data(index - this->stride(i)).spin;
    }

    int local_spin = this->data(index).spin;

    return 1.0 * J * local_spin * neighbour_spins_sum + h * local_spin;
}

template <size_t Rank>
float Ising<Rank>::local_update_energy_change(const size_t &index) const {
    return -2.0 * local_hamiltonian(index);
}

/* Update spins according to the Boltzmann probability distribution */
template <size_t Rank> void Ising<Rank>::update() {
    for (int i = 0; i < this->size(); i++) {
        float dE = local_update_energy_change(i);
        float acceptance_probability = exp(-1.0 * B * dE);
        if (((float)rand() / (float)RAND_MAX) < acceptance_probability) {
            this->data(i).spin *= -1;
        }
    }
}

/* TODO swap if and for statements */
template <size_t Rank>
void Ising<Rank>::hamiltonian(int dim, std::vector<int> &index,
                              float &H) const {
    for (index[dim] = this->buffer_width();
         index[dim] < this->dimension_size(dim) - this->buffer_width();
         index[dim]++) {
        if (dim == 0) {
            H += local_hamiltonian(this->index(index));
        } else {
            hamiltonian(dim - 1, index, H);
        }
    }
}

/* TODO maybe calculate this as a checkerboard scheme */
template <size_t Rank> float Ising<Rank>::hamiltonian() const {
    float H = 0.0;
    std::vector<int> index(m_rank);

    hamiltonian(m_rank - 1, index, H);

    return H / 2.0; /* each neighbour pair is accounted for twice */
}

template <size_t Rank>
void Ising<Rank>::print(int dim, std::vector<int> &index) const {
    for (index[dim] = this->buffer_width();
         index[dim] < this->dimension_size(dim) - this->buffer_width();
         index[dim]++) {
        if (dim == 0) {
            std::cout << this->value(index).spin << " ";
        } else {
            print(dim - 1, index);
        }
    }
    std::cout << std::endl;
}

template <size_t Rank> void Ising<Rank>::print() const {
    std::cout << "Dimensions: ";
    for (int i = 0; i < m_rank; i++) {
        std::cout << this->dimension_size(i) - 2*this->buffer_width() << " ";
    }
    std::cout << std::endl << std::endl;

    std::vector<int> index(m_rank);
    print(m_rank - 1, index);
}

} // namespace field
} // namespace latticeqft
