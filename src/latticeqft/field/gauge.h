#include "field.h"

namespace latticeqft {
namespace field {

typedef struct gauge_variable {
    float phi; /* [0, 2pi] */
    float re;  /* cos(phi) */
    float im;  /* sin(phi) */
} Gauge_variable;

template <size_t Rank> class Gauge : public Edges<Gauge_variable, Rank> {
  public:
    static constexpr const size_t m_rank = Rank;

  public:
    Gauge() {}

    template <typename X>
    Gauge(const std::array<X, m_rank> &sizes)
        : Edges<Gauge_variable, m_rank>(sizes) {}

    template <typename X>
    Gauge(const std::vector<X> &sizes) : Edges<Gauge_variable, m_rank>(sizes) {}

    void initialise() override;
    void update() override;
};

template <size_t Rank> void Gauge<Rank>::initialise() {}

template <size_t Rank> void Gauge<Rank>::update() {}

} // namespace field
} // namespace latticeqft
