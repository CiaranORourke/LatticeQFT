#pragma once

#include "nd_vector.h"

namespace latticeqft {
namespace field {

template <typename T, size_t Rank> class Field : public Nd_vector<T, Rank> {
  public:
    static constexpr const size_t m_rank = Rank;

  private:
    unsigned int m_buffer_width = 1;

    /* number of boundaries a vertex lies on */
    Nd_vector<int, m_rank> m_num_boundaries;

  public:
    Field() {}

    const unsigned int buffer_width() const { return m_buffer_width; }

    template <typename X>
    Field(const std::array<X, m_rank> &sizes) : Nd_vector<T, m_rank>(sizes) {}

    template <typename X>
    Field(const std::vector<X> &sizes) : Nd_vector<T, m_rank>(sizes) {}

    virtual void initialise() = 0;
    virtual void update() = 0;
};

template <typename T, size_t Rank> class Vertices : public Field<T, Rank> {
  public:
    Vertices() {}

    template <typename X>
    Vertices(const std::array<X, Rank> &sizes) : Field<T, Rank>(sizes) {}

    template <typename X>
    Vertices(const std::vector<X> &sizes) : Field<T, Rank>(sizes) {}
};

/* TODO maybe change iheritance to <T, Rank + 1> */
template <typename T, size_t Rank>
class Edges : public Field<std::array<T, Rank>, Rank> {
  public:
    Edges() {}

    template <typename X>
    Edges(const std::array<X, Rank> &sizes) : Field<T, Rank>(sizes) {}

    template <typename X>
    Edges(const std::vector<X> &sizes) : Field<T, Rank>(sizes) {}
};

} // namespace field
} // namespace latticeqft
