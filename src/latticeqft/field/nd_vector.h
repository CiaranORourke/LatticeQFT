#include <array>
#include <cassert>
#include <vector>

namespace latticeqft {
namespace field {

template <typename T> constexpr T multiply(T t) { return t; }

template <typename T, typename... Ts> constexpr T multiply(T t, Ts... ts) {
    return t * multiply<T>(ts...);
}

template <typename T, size_t Rank>
constexpr T multiply(const std::array<T, Rank> &values) {
    T ret = values[0];
    for (size_t i = 1; i < values.size(); i++) {
        ret *= values[i];
    }
    return ret;
}

template <typename T> constexpr T multiply(const std::vector<T> &values) {
    T ret = values[0];
    for (size_t i = 1; i < values.size(); i++) {
        ret *= values[i];
    }
    return ret;
}

template <typename T, size_t Rank> class Nd_vector {
  public:
    static constexpr const size_t m_rank = Rank;

  private:
    std::array<size_t, m_rank> m_dimension_sizes;
    std::vector<T> m_data;

    std::array<size_t, m_rank> m_strides;

  public:
    Nd_vector() {}

    template <typename X>
    Nd_vector(const std::array<X, m_rank> &sizes)
        : m_data(multiply<X, m_rank>(sizes)) {
        for (size_t i = 0; i < m_rank; i++) {
            m_dimension_sizes[i] = static_cast<size_t>(sizes[i]);
        }

        m_strides[0] = 1;
        for (size_t i = 1; i < m_rank; i++) {
            m_strides[i] = m_strides[i - 1] * m_dimension_sizes[i - 1];
        }
    }

    template <typename X>
    Nd_vector(const std::vector<X> &sizes) : m_data(multiply<X>(sizes)) {
        for (size_t i = 0; i < m_rank; i++) {
            m_dimension_sizes[i] = static_cast<size_t>(sizes[i]);
        }

        m_strides[0] = 1;
        for (size_t i = 1; i < m_rank; i++) {
            m_strides[i] = m_strides[i - 1] * m_dimension_sizes[i - 1];
        }
    }

    ~Nd_vector() {}

    Nd_vector(const Nd_vector<T, m_rank> &input)
        : m_dimension_sizes(input.m_dimension_sizes), m_data(input.m_data) {}

    Nd_vector &operator=(const Nd_vector<T, m_rank> &input) {
        if (this != &input) {
            m_dimension_sizes = input.m_dimension_sizes;
            m_data = input.m_data;
        }
        return *this;
    }

    Nd_vector(Nd_vector<T, m_rank> &&input)
        : m_dimension_sizes(std::move(input.m_dimension_sizes)),
          m_data(std::move(input.m_data)) {}

    Nd_vector &operator=(Nd_vector<T, m_rank> &&input) {
        if (this != &input) {
            m_dimension_sizes = std::move(input.m_dimension_sizes);
            m_data = std::move(input.m_data);
        }
        return *this;
    }

    void clear() { m_data.clear(); }

    template <typename X> void resize(const std::array<X, m_rank> &sizes) {
        for (size_t i = 0; i < m_rank; i++) {
            m_dimension_sizes[i] = static_cast<size_t>(sizes[i]);
        }

        m_data.resize(multiply(sizes));

        m_strides[0] = 1;
        for (size_t i = 1; i < m_rank; i++) {
            m_strides[i] = m_strides[i - 1] * m_dimension_sizes[i - 1];
        }
    }

    template <typename X> void resize(const std::vector<X> &sizes) {
        for (size_t i = 0; i < m_rank; i++) {
            m_dimension_sizes[i] = static_cast<size_t>(sizes[i]);
        }

        m_data.resize(multiply<X>(sizes));

        m_strides[0] = 1;
        for (size_t i = 1; i < m_rank; i++) {
            m_strides[i] = m_strides[i - 1] * m_dimension_sizes[i - 1];
        }
    }

    template <typename X>
    inline size_t index(const std::array<X, m_rank> &x_array) const {
        size_t lexicographical_index = 0;
        for (size_t i = 0; i < m_rank; i++) {
            lexicographical_index += x_array[i] * m_strides[i];
        }

        return lexicographical_index;
    }

    template <typename X>
    inline size_t index(const std::vector<X> &x_array) const {
        size_t lexicographical_index = 0;
        for (size_t i = 0; i < m_rank; i++) {
            lexicographical_index += x_array[i] * m_strides[i];
        }

        return lexicographical_index;
    }

    template <typename X>
    const T &value(const std::array<X, m_rank> &x_array) const {
        return m_data[index(x_array)];
    }

    template <typename X> T &value(const std::array<X, m_rank> &x_array) {
        return m_data[index(x_array)];
    }

    template <typename X> const T &value(const std::vector<X> &x_array) const {
        return m_data[index(x_array)];
    }

    template <typename X> T &value(const std::vector<X> &x_array) {
        return m_data[index(x_array)];
    }

    size_t rank() const { return m_rank; }

    const std::array<size_t, m_rank> &dimension_sizes() const {
        return m_dimension_sizes;
    }

    size_t dimension_size(size_t dim) const { return m_dimension_sizes[dim]; }

    const std::array<size_t, m_rank> &strides() const { return m_strides; }

    size_t stride(size_t dim) const { return m_strides[dim]; }

    size_t size() const { return m_data.size(); }

    const T *data() const { return m_data.data(); }

    T &data() { return m_data.data(); }

    const T &data(size_t x) const { return m_data[x]; }

    T &data(size_t x) { return m_data[x]; }

    const T &operator[](size_t x) const { return m_data[index]; }

    T &operator[](size_t x) { return m_data[index]; }
};

} // namespace field
} // namespace latticeqft
