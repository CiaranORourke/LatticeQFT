#!/usr/bin/gnuplot


D = 2
L = 2048


infile1(D, L) = sprintf("scaling/ising-%dd-weak.txt", D)
infile2(D, L) = sprintf("scaling/ising-%dd-%d-strong.txt", D, L)
outfile1(D, L) = sprintf("plots/ising-%dd-weak.ps", D)
outfile2(D, L) = sprintf("plots/ising-%dd-strong.ps", D)

title1(D, L) = sprintf("D=%d, L=%d per core: Weak Scaling of Ising Model", D, L)
title2(D, L) = sprintf("D=%d, L=%d: Stong Scaling of Ising Model", D, L)


set terminal unknown
set output "dump.txt"
plot infile2(D, L) using 1:2


YMAX=GPVAL_Y_MAX

ideal(N) = YMAX/N

set terminal postscript font "Helvetica,25"

set output outfile2(D, L)


set title title2(D, L)
set xlabel "Number of Processors"
set ylabel "Time taken (s)"
set logscale x
set logscale y
#set key outside
set autoscale

plot infile2(D, L) using 1:2 pt 3 ps 8 title "",\
        ideal(x) with line title "ideal"

set terminal unknown
set output "dump.txt"

unset logscale y

plot infile1(D, L) using 1:2

YMIN=GPVAL_Y_MIN

set terminal postscript font "Helvetica,25"
#unset logscale y
set output outfile1(D, L)
set title title1(D, L)
set ylabel "Parallel Efficiency (%)"
set yrange [0:]

plot infile1(D, L) using 1:(100*YMIN/$2) pt 3 ps 8 title ""
