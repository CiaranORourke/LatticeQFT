#!/usr/bin/gnuplot

set terminal postscript font "Helvetica,25"

D = 4
L = 20

infile(D, L) = sprintf("data/gauge_sim.txt")
outfile1(D, L) = sprintf("plots/%dd-%d-gauge_therm.ps", D, L)
outfile2(D, L) = sprintf("plots/%dd-%d-gauge_delta_therm.ps", D, L)

title1(D, L) = sprintf("D=%d, L=%d: Thermalisation of U(1) Gauge Theory - Action per Plaquette", D, L)
title2(D, L) = sprintf("D=%d, L=%d: Thermalisation of U(1) Gauge Theory - Change in Action per Plaquette", D, L)

set output outfile1(D, L)

set title title1(D, L)
set xlabel "T"
set ylabel "U_P"
set key outside
set autoscale

plot infile(D, L) using 1:2 with linespoints title ""

set output outfile2(D, L)

set title title2(D, L)
set ylabel "dS_P"

plot infile(D, L) using 1:3 with linespoints title ""

