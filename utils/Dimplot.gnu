#!/usr/bin/gnuplot

set terminal postscript font "Helvetica,25"

V = 4096


infile = sprintf("scaling/ising-dims.txt")
outfile(V) = sprintf("plots/ising-%d-dims.ps", V)

title1(V) = sprintf("V=%d: Dimensionality Scaling of Ising Model", V)

set output outfile(V)

set title title1(V)
set xlabel "Number of Dimensions"
set ylabel "Time taken (s)"
set key outside
set autoscale

plot infile using 1:2 pt 3 ps 8 title ""
