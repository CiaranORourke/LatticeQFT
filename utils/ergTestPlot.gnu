#!/usr/bin/gnuplot

set terminal postscript font "Helvetica,25"

D = 4
L = 16
T = 1.0

infile(D, L) = sprintf("test/gauge-%dd-%d-metro.txt", D, L)
outfile1(D, L) = sprintf("plots/gauge-%dd-%d-%.1f-metro-test.ps", D, L, T)

title1(D, L) = sprintf("D=%d, L=%d, Beta==%.1f: Test for Ergodicity", D, L, T)

set output outfile1(D, L)

set title title1(D, L)
set xlabel "N"
set ylabel "e^(-Beta*dS)"
set key outside
set autoscale

plot infile(D, L) using 1:2 with linespoints title ""

