#!/usr/bin/gnuplot

set terminal postscript

D = 2
L = 8

infile(D, L) = sprintf("data/%dd-%d-tran.txt", D, L)
outfile1(D, L) = sprintf("plots/%dd-%d-tran.ps", D, L)

title1(D, L) = sprintf("D=%d, L=%d: Initial System Energy v Transient length", D, L)

set output outfile1(D, L)

set title title1(D, L)
set xlabel "Metropolis Loops"
set ylabel "E"
set logscale x
set key outside
set autoscale

plot infile(D, L) using 1:2 with linespoints title "T = 0.5", \
	infile(D, L) using 1:3 with linespoints title "T = 2.5", \
	infile(D, L) using 1:4 with linespoints title "T = 5.0"

