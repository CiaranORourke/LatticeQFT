#!/usr/bin/gnuplot

set terminal postscript font "Helvetica,25"

D = 2
L = 8


infile(D, L) = sprintf("data/%dd-%d-data.txt", D, L)
outfile1(D, L) = sprintf("plots/%dd-%d-EvT.ps", D, L)
outfile2(D, L) = sprintf("plots/%dd-%d-MvT.ps", D, L)

title1(D, L) = sprintf("D=%d, L=%d: Total System Energy v Temperature", D, L)
title2(D, L) = sprintf("D=%d, L=%d: |Total System Magnetisation| v Temperature", D, L)

set output outfile1(D, L)

set title title1(D, L)
set xlabel "T"
set ylabel "E"
set key outside
set autoscale

plot infile(D, L) using 1:2:(2*sqrt($3)) with yerrorbars title ""

set output outfile2(D, L)

set title title2(D, L)
set ylabel "M"

plot infile(D, L) using 1:4:(2*sqrt($5)) with yerrorbars title ""
